package com.example.homework2.di.user

import com.example.homework2.di.AppComponent
import com.example.homework2.ui.features.profile.UserProfileFragment
import dagger.Component

@UserScope
@Component(
    dependencies = [AppComponent::class]
)
interface UserComponent {

    fun inject(userProfileFragment: UserProfileFragment)

    companion object {
        fun init(appComponent: AppComponent): UserComponent =
            DaggerUserComponent.builder()
                .appComponent(appComponent)
                .build()
    }
}