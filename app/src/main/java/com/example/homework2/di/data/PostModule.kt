package com.example.homework2.di.data

import com.example.homework2.data.repository.comments.CommentRepository
import com.example.homework2.data.repository.comments.CommentRepositoryImpl
import com.example.homework2.data.repository.likes.LikesRepository
import com.example.homework2.data.repository.likes.LikesRepositoryImpl
import com.example.homework2.data.repository.news.NewsRepository
import com.example.homework2.data.repository.news.NewsRepositoryImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface PostModule {

    @Binds
    @Singleton
    fun likesRepository(likesRepository: LikesRepositoryImpl): LikesRepository

    @Binds
    @Singleton
    fun postRepository(postRepository: NewsRepositoryImpl): NewsRepository

    @Binds
    @Singleton
    fun commentRepository(commentRepository: CommentRepositoryImpl): CommentRepository
}