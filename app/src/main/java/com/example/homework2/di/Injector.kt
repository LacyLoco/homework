package com.example.homework2.di

import com.example.homework2.di.create_post.CreatePostComponent
import com.example.homework2.di.favorite_posts.FavoritesPostsComponent
import com.example.homework2.di.main.MainComponent
import com.example.homework2.di.news.NewsComponent
import com.example.homework2.di.post.PostComponent
import com.example.homework2.di.user.UserComponent

interface Injector {
    fun appComponent(): AppComponent

    fun mainComponent(): MainComponent
    fun releaseMainComponent()

    fun postComponent(): PostComponent
    fun releasePostComponent()

    fun newsComponent(): NewsComponent
    fun releaseNewsComponent()

    fun favoritesPostsComponent(): FavoritesPostsComponent
    fun releaseFavoritesPostsComponent()

    fun userComponent(): UserComponent
    fun releaseUserComponent()

    fun createPostComponent(): CreatePostComponent
    fun releaseCreatePostComponent()
}