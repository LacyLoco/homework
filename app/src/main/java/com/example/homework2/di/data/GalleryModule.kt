package com.example.homework2.di.data

import com.example.homework2.data.repository.gallery.Gallery
import com.example.homework2.data.repository.gallery.GalleryImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface GalleryModule {

    @Binds
    @Singleton
    fun gallery(gallery: GalleryImpl): Gallery
}