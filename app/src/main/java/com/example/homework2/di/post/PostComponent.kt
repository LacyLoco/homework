package com.example.homework2.di.post

import com.example.homework2.di.AppComponent
import com.example.homework2.ui.features.post_details.PostFragment
import dagger.Component

@PostScope
@Component(
    dependencies = [AppComponent::class]
)
interface PostComponent {

    fun inject(postFragment: PostFragment)

    companion object {
        fun init(appComponent: AppComponent): PostComponent =
            DaggerPostComponent.builder()
                .appComponent(appComponent)
                .build()
    }
}