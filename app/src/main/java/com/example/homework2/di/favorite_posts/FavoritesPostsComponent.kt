package com.example.homework2.di.favorite_posts

import com.example.homework2.di.AppComponent
import com.example.homework2.ui.features.favorite_post_list.FavoritePostsFragment
import dagger.Component

@FavoritePostsScope
@Component(
    dependencies = [AppComponent::class]
)
interface FavoritesPostsComponent {

    fun inject(favoritePostsFragment: FavoritePostsFragment)

    companion object {
        fun init(appComponent: AppComponent): FavoritesPostsComponent =
            DaggerFavoritesPostsComponent.builder()
                .appComponent(appComponent)
                .build()
    }
}