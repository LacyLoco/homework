package com.example.homework2.di.data.network

import com.example.homework2.BuildConfig
import com.example.homework2.data.api.ApiInterceptor
import com.example.homework2.data.repository.storage.UserStorageRepository
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import javax.inject.Singleton

@Module
class OkHttpModule {

    @Provides
    @Singleton
    fun provideOkHttpClient(apiInterceptor: ApiInterceptor): OkHttpClient =
        OkHttpClient.Builder().apply {
            addInterceptor(apiInterceptor)
            if (BuildConfig.DEBUG)
                addInterceptor(createLoggingInterceptor())
        }.build()

    private fun createLoggingInterceptor(): Interceptor =
        HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

    @Provides
    @Singleton
    fun provideApiInterceptor(
        userStorageRepository: UserStorageRepository,
        gson: Gson
    ): ApiInterceptor =
        ApiInterceptor(userStorageRepository, gson)
}