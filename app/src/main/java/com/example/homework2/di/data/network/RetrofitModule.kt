package com.example.homework2.di.data.network

import com.example.homework2.data.api.ApiUrl
import com.example.homework2.data.type_adapter.BooleanTypeAdapter
import com.example.homework2.data.type_adapter.DateTypeAdapter
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import javax.inject.Singleton

@Module(includes = [OkHttpModule::class])
class RetrofitModule {

    @Provides
    @Singleton
    fun provideRetrofit(builder: Retrofit.Builder): Retrofit =
        builder.baseUrl(ApiUrl.BASE_URL).build()

    @Provides
    @Singleton
    fun provideRetrofitBuilder(okHttpClient: OkHttpClient): Retrofit.Builder =
        Retrofit.Builder()
            .baseUrl(ApiUrl.BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(createGsonConverterFactory())
            .client(okHttpClient)

    private fun createGsonConverterFactory() =
        GsonConverterFactory.create(
            GsonBuilder()
                .setLenient()
                .registerTypeAdapter(
                    Boolean::class.javaObjectType,
                    BooleanTypeAdapter
                )
                .registerTypeAdapter(
                    Calendar::class.javaObjectType,
                    DateTypeAdapter
                )
                .create()
        )

    @Provides
    @Singleton
    fun provideGson(): Gson =
        Gson()
}