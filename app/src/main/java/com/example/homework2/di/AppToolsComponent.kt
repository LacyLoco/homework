package com.example.homework2.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppToolsComponent : AppToolsProvider {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun app(app: Application): Builder
        fun build(): AppToolsComponent
    }

    companion object {
        fun init(app: Application): AppToolsProvider =
            DaggerAppToolsComponent.builder()
                .app(app)
                .build()
    }
}