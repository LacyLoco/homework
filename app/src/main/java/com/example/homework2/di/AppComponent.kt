package com.example.homework2.di

import android.app.Application
import com.example.homework2.App
import com.example.homework2.di.data.DataComponent
import com.example.homework2.di.data.DataToolsProvider
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    dependencies = [
        AppToolsProvider::class,
        DataToolsProvider::class
    ]
)
interface AppComponent : DataToolsProvider {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder
        fun appTools(appToolsProvider: AppToolsProvider): Builder
        fun dataTools(dataToolsProvider: DataToolsProvider): Builder
        fun build(): AppComponent
    }

    fun inject(app: App)

    companion object {
        fun init(app: Application): AppComponent {
            val appToolsProvider = AppToolsComponent.init(app)
            val dataToolsProvider = DataComponent.init(appToolsProvider)

            return DaggerAppComponent.builder()
                .application(app)
                .appTools(appToolsProvider = appToolsProvider)
                .dataTools(dataToolsProvider = dataToolsProvider)
                .build()
        }
    }
}