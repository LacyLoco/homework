package com.example.homework2.di.data

import com.example.homework2.di.AppToolsProvider
import com.example.homework2.di.data.db.DBModule
import com.example.homework2.di.data.network.ApiModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    dependencies = [AppToolsProvider::class],
    modules = [
        ApiModule::class,
        DBModule::class,
        PostModule::class,
        StorageModule::class,
        GalleryModule::class,
        UserModule::class
    ]
)
interface DataComponent : DataToolsProvider {
    companion object {
        fun init(appToolsProvider: AppToolsProvider): DataToolsProvider =
            DaggerDataComponent.builder()
                .appToolsProvider(appToolsProvider)
                .build()
    }
}