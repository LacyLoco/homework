package com.example.homework2.di

import android.app.Application
import com.example.homework2.di.create_post.CreatePostComponent
import com.example.homework2.di.favorite_posts.FavoritesPostsComponent
import com.example.homework2.di.main.MainComponent
import com.example.homework2.di.news.NewsComponent
import com.example.homework2.di.post.PostComponent
import com.example.homework2.di.user.UserComponent

object ComponentHolder : Injector {

    private lateinit var appComponent: AppComponent

    private var mainComponent: MainComponent? = null
    private var postComponent: PostComponent? = null
    private var newsComponent: NewsComponent? = null
    private var favoritesPostsComponent: FavoritesPostsComponent? = null
    private var userComponent: UserComponent? = null
    private var createPostComponent: CreatePostComponent? = null

    fun initAppComponent(app: Application) {
        appComponent = AppComponent.init(app)
    }

    override fun appComponent(): AppComponent = appComponent

    override fun mainComponent(): MainComponent =
        mainComponent ?: MainComponent.init(appComponent)
            .also { mainComponent = it }

    override fun releaseMainComponent() {
        mainComponent = null
        releaseMainSubComponents()
    }

    override fun postComponent(): PostComponent =
        postComponent ?: PostComponent.init(appComponent)
            .also { postComponent = it }

    override fun releasePostComponent() {
        postComponent = null
    }

    override fun newsComponent(): NewsComponent =
        newsComponent ?: NewsComponent.init(appComponent)
            .also { newsComponent = it }

    override fun releaseNewsComponent() {
        newsComponent = null
    }

    override fun favoritesPostsComponent(): FavoritesPostsComponent =
        favoritesPostsComponent ?: FavoritesPostsComponent.init(appComponent)
            .also { favoritesPostsComponent = it }

    override fun releaseFavoritesPostsComponent() {
        favoritesPostsComponent = null
    }

    override fun userComponent(): UserComponent =
        userComponent ?: UserComponent.init(appComponent)
            .also { userComponent = it }

    override fun releaseUserComponent() {
        userComponent = null
    }

    override fun createPostComponent(): CreatePostComponent =
        createPostComponent ?: CreatePostComponent.init(appComponent)
            .also { createPostComponent = it }

    override fun releaseCreatePostComponent() {
        createPostComponent = null
    }

    private fun releaseMainSubComponents() {
        releaseNewsComponent()
        releaseFavoritesPostsComponent()
        releaseUserComponent()
    }
}