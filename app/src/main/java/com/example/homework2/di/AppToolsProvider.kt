package com.example.homework2.di

import android.content.Context

interface AppToolsProvider {
    fun provideContext(): Context
}