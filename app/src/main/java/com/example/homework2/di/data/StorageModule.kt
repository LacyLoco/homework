package com.example.homework2.di.data

import com.example.homework2.data.repository.storage.UserStorageRepository
import com.example.homework2.data.repository.storage.UserStorageRepositoryImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface StorageModule {

    @Binds
    @Singleton
    fun userStorageRepository(userStorageRepository: UserStorageRepositoryImpl): UserStorageRepository
}