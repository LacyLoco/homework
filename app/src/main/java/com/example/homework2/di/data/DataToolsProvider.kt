package com.example.homework2.di.data

import com.example.homework2.data.database.DataBaseController
import com.example.homework2.data.repository.comments.CommentRepository
import com.example.homework2.data.repository.gallery.Gallery
import com.example.homework2.data.repository.likes.LikesRepository
import com.example.homework2.data.repository.news.NewsRepository
import com.example.homework2.data.repository.storage.UserStorageRepository
import com.example.homework2.data.repository.user.UserProfileRepository
import com.example.homework2.data.repository.wall.UserWallRepository

interface DataToolsProvider {
    fun provideLikeRepository(): LikesRepository
    fun provideNewsRepository(): NewsRepository
    fun provideDataBaseController(): DataBaseController
    fun provideUserStorageRepository(): UserStorageRepository
    fun provideGallery(): Gallery
    fun provideCommentRepository(): CommentRepository
    fun provideUserProfileRepository(): UserProfileRepository
    fun provideUserWallRepository(): UserWallRepository
}