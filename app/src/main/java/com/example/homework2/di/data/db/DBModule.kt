package com.example.homework2.di.data.db

import android.content.Context
import androidx.room.Room
import com.example.homework2.data.database.AppDataBase
import com.example.homework2.data.database.DataBaseController
import com.example.homework2.data.database.DataBaseControllerImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DBModule {

    companion object {
        private const val DATABASE_NAME = "app_database"
    }

    @Provides
    @Singleton
    fun provideDB(context: Context): AppDataBase =
        Room.databaseBuilder(
            context,
            AppDataBase::class.java,
            DATABASE_NAME
        )
            .fallbackToDestructiveMigration()
            .build()

    @Provides
    @Singleton
    fun provideDBController(appDataBase: AppDataBase): DataBaseController =
        DataBaseControllerImpl(appDataBase)
}