package com.example.homework2.di.news

import com.example.homework2.di.AppComponent
import com.example.homework2.ui.features.news.NewsFragment
import dagger.Component

@NewsScope
@Component(
    dependencies = [AppComponent::class]
)
interface NewsComponent {

    fun inject(newsFragment: NewsFragment)

    companion object {
        fun init(appComponent: AppComponent): NewsComponent =
            DaggerNewsComponent.builder()
                .appComponent(appComponent)
                .build()
    }
}