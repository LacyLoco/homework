package com.example.homework2.di.main

import com.example.homework2.di.AppComponent
import com.example.homework2.ui.features.main.MainActivity
import dagger.Component

@MainScope
@Component(
    dependencies = [AppComponent::class]
)
interface MainComponent {

    fun inject(mainActivity: MainActivity)

    companion object {
        fun init(appComponent: AppComponent): MainComponent =
            DaggerMainComponent.builder()
                .appComponent(appComponent)
                .build()
    }
}