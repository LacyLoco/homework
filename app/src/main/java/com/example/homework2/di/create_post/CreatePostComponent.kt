package com.example.homework2.di.create_post

import com.example.homework2.di.AppComponent
import com.example.homework2.ui.features.create_post.CreatePostFragment
import dagger.Component

@CreatePostScope
@Component(
    dependencies = [AppComponent::class]
)
interface CreatePostComponent {

    fun inject(createPostFragment: CreatePostFragment)

    companion object {
        fun init(appComponent: AppComponent): CreatePostComponent =
            DaggerCreatePostComponent.builder()
                .appComponent(appComponent)
                .build()
    }
}