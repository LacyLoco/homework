package com.example.homework2.di.data

import com.example.homework2.data.repository.user.UserProfileRepository
import com.example.homework2.data.repository.user.UserProfileRepositoryImpl
import com.example.homework2.data.repository.wall.UserWallRepository
import com.example.homework2.data.repository.wall.UserWallRepositoryImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface UserModule {

    @Binds
    @Singleton
    fun userProfileRepository(userProfileRepository: UserProfileRepositoryImpl): UserProfileRepository

    @Binds
    @Singleton
    fun userWallRepository(userWallRepository: UserWallRepositoryImpl): UserWallRepository
}