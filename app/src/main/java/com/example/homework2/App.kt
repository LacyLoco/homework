package com.example.homework2

import android.app.Application
import com.example.homework2.data.database.DataBaseController
import com.example.homework2.di.ComponentHolder
import com.example.homework2.di.Injector
import javax.inject.Inject

class App : Application(), Injector by ComponentHolder {

    @Inject
    lateinit var dataBaseController: DataBaseController

    override fun onCreate() {
        super.onCreate()
        ComponentHolder.initAppComponent(this)
        appComponent().inject(this)
        dataBaseController.checkRelevanceOfData()
    }
}