package com.example.homework2.ui.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity<View, Presenter : BasePresenter<View>> : AppCompatActivity() {

    abstract var presenter: Presenter

    @Suppress("UNCHECKED_CAST")
    override fun onCreate(savedInstanceState: Bundle?) {
        injectComponent()
        super.onCreate(savedInstanceState)
        presenter.attachView(this as View)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView(isFinishing)
        if (isFinishing)
            releaseComponent()
    }

    open fun injectComponent() = Unit
    open fun releaseComponent() = Unit
}