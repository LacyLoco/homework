package com.example.homework2.ui.screen

interface HomeScreen : AuthListener

interface FavoriteScreen

interface UserProfileScreen

interface AuthListener {
    fun authSuccess()
}