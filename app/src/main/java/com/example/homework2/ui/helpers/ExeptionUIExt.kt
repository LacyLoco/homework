package com.example.homework2.ui.helpers

import android.content.Context
import com.example.homework2.R
import com.example.homework2.data.converter.AuthException
import com.example.homework2.data.converter.ConvertDataException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

fun Throwable.convertToString(context: Context): String =
    when (this) {
        is UnknownHostException -> context.getString(R.string.unreachableHostError)
        is AuthException -> context.getString(R.string.authIsRequiredError)
        is ConvertDataException -> context.getString(R.string.convertError)
        is SocketTimeoutException -> context.getString(R.string.socketTimeoutError)
        else -> this.localizedMessage ?: context.getString(R.string.unknownError)
    }