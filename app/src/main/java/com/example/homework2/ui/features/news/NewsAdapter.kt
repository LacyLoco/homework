package com.example.homework2.ui.features.news

import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ProgressBar
import androidx.recyclerview.widget.ListAdapter
import com.example.homework2.extension.dp
import com.example.homework2.model.Post
import com.example.homework2.model.ui.Error
import com.example.homework2.model.ui.FullScreenLoader
import com.example.homework2.model.ui.Loader
import com.example.homework2.model.ui.Retry
import com.example.homework2.ui.TransitionParams
import com.example.homework2.ui.features.base.holders.*
import com.example.homework2.ui.helpers.ItemDiffer
import com.example.homework2.ui.screen.RecyclerItem
import com.example.homework2.ui.view.ErrorView
import com.example.homework2.ui.view.LoadingPostShimmerView
import com.example.homework2.ui.view.PostView
import com.example.homework2.ui.view.PostWithImagesView

class NewsAdapter(
    private val itemClick: (Post, View) -> Unit,
    private val commentClick: (Post, View) -> Unit,
    private val repostClick: (Post) -> Unit,
    private val saveImageClick: (Post) -> Unit,
    private val retryClick: () -> Unit
) : ListAdapter<RecyclerItem, BaseHolder<*>>(ItemDiffer) {

    fun getDecoratorTextForPosition(position: Int): String {
        val currentPost = currentList.getOrNull(position) as? Post
        val previewPost = currentList.getOrNull(position - 1) as? Post

        return if (currentPost?.textDate != previewPost?.textDate) currentPost?.textDate
            ?: "" else ""
    }

    override fun getItemViewType(position: Int): Int =
        when (val item = currentList[position]) {
            is Post -> getPostType(item)
            is Loader -> LOADER_VIEW
            is FullScreenLoader -> LOADER_SHIMMER_VIEW
            is Error -> ERROR_VIEW
            is Retry -> RETRY_VIEW
            else -> throw IllegalArgumentException("undefined view type")
        }

    private fun getPostType(post: Post) =
        when (post.image) {
            null -> POST_VIEW
            else -> POST_IMAGE_VIEW
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseHolder<*> =
        when (viewType) {
            POST_VIEW -> initTextViewHolder(parent)
            POST_IMAGE_VIEW -> initTextWithImageViewHolder(parent)
            LOADER_VIEW -> initLoaderViewHolder(parent)
            ERROR_VIEW -> initErrorViewHolder(parent)
            LOADER_SHIMMER_VIEW -> initLoaderShimmerViewHolder(parent)
            RETRY_VIEW -> RetryViewHolder.create(parent.context, retryClick)
            else -> throw IllegalArgumentException("unresolved viewType: $viewType")
        }

    override fun onBindViewHolder(
        holder: BaseHolder<*>,
        position: Int,
        payloads: MutableList<Any>
    ) {
        payloads.firstOrNull()?.let {
            (holder as? BasePostViewHolder)?.bind((currentList[position] as Post).likes)
        } ?: super.onBindViewHolder(holder, position, payloads)
    }

    override fun onBindViewHolder(holder: BaseHolder<*>, position: Int) {
        holder._item = currentList[position]
        holder.itemView.transitionName = TransitionParams.postTransitionName(position)
        when (holder) {
            is TextViewHolder -> holder.bind(currentList[position] as Post)
            is TextWithImageViewHolder -> holder.bind(currentList[position] as Post)
        }
    }

    private fun initTextViewHolder(parent: ViewGroup) =
        TextViewHolder(PostView(parent.context), commentClick, itemClick)

    private fun initTextWithImageViewHolder(parent: ViewGroup) =
        TextWithImageViewHolder(
            PostWithImagesView(parent.context),
            itemClick,
            commentClick,
            repostClick,
            saveImageClick
        )

    private fun initLoaderShimmerViewHolder(parent: ViewGroup) =
        LoaderShimmerViewHolder(
            LoadingPostShimmerView(parent.context).apply {
                layoutParams = FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.MATCH_PARENT,
                    FrameLayout.LayoutParams.MATCH_PARENT
                )
            }
        )

    private fun initErrorViewHolder(parent: ViewGroup) =
        ErrorViewHolder(
            ErrorView(parent.context).apply {
                layoutParams = FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.MATCH_PARENT,
                    FrameLayout.LayoutParams.MATCH_PARENT
                )
            }
        )

    private fun initLoaderViewHolder(parent: ViewGroup) =
        LoaderViewHolder(
            ProgressBar(parent.context).apply {
                layoutParams = FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.MATCH_PARENT,
                    32.dp()
                )
            }
        )

    companion object {
        const val POST_VIEW = 0
        const val POST_IMAGE_VIEW = 1
        const val LOADER_VIEW = 2
        const val ERROR_VIEW = 3
        const val LOADER_SHIMMER_VIEW = 4
        const val RETRY_VIEW = 5
    }
}