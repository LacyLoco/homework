package com.example.homework2.ui.features.main

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.core.view.ViewCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Lifecycle
import com.example.homework2.R
import com.example.homework2.extension.injector
import com.example.homework2.model.Post
import com.example.homework2.ui.base.BaseActivity
import com.example.homework2.ui.features.create_post.CreatePostFragment
import com.example.homework2.ui.features.favorite_post_list.FavoritePostsFragment
import com.example.homework2.ui.features.main.RouterAnimationHelper.applyFavoriteScreenAnimation
import com.example.homework2.ui.features.main.RouterAnimationHelper.applyHomeScreenAnimation
import com.example.homework2.ui.features.main.RouterAnimationHelper.applyUserProfileScreenAnimation
import com.example.homework2.ui.features.news.NewsFragment
import com.example.homework2.ui.features.post_details.PostFragment
import com.example.homework2.ui.features.profile.UserProfileFragment
import com.example.homework2.ui.router.MainRouter
import com.example.homework2.ui.screen.AuthListener
import com.example.homework2.ui.screen.FullScreen
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.vk.api.sdk.VK
import com.vk.api.sdk.auth.VKAccessToken
import com.vk.api.sdk.auth.VKAuthCallback
import com.vk.api.sdk.auth.VKScope
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject
import kotlin.reflect.KClass

class MainActivity : BaseActivity<MainView, MainPresenter>(),
    BottomNavigationView.OnNavigationItemSelectedListener,
    MainView,
    MainRouter {

    @Inject
    override lateinit var presenter: MainPresenter

    private val newsFragment: NewsFragment by lazy {
        findFragment(NewsFragment::class) ?: NewsFragment.newInstance()
    }
    private val favoritePostsFragment: FavoritePostsFragment by lazy {
        findFragment(FavoritePostsFragment::class) ?: FavoritePostsFragment.newInstance()
    }
    private val userProfileFragment: UserProfileFragment by lazy {
        findFragment(UserProfileFragment::class) ?: UserProfileFragment.newInstance()
    }

    override fun injectComponent() =
        injector.mainComponent().inject(this)

    override fun releaseComponent() =
        injector.releaseMainComponent()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //init lazy fields
        newsFragment
        favoritePostsFragment
        userProfileFragment

        supportFragmentManager.addOnBackStackChangedListener {
            val lastFragment = supportFragmentManager.fragments.lastOrNull()
            navigation.isVisible = lastFragment !is FullScreen
        }

        navigation.setOnNavigationItemSelectedListener(this)

        presenter.input.accept(Action.CheckHasFavoritePosts)

        if (savedInstanceState == null)
            showMainScreen()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            R.id.navigation_news -> showNewsFragment()
            R.id.navigation_favorites -> showFavoritePostsFragment()
            R.id.navigation_profile -> showUserProfileFragment()
            else -> false
        }

    override fun showPostDetailsScreen(post: Post, sharedView: View) {
        val transitionName = ViewCompat.getTransitionName(sharedView).orEmpty()
        showPostScreen(post, sharedView, transitionName, PostFragment.ShowType.POST)
    }

    override fun showPostCommentsScreen(post: Post, sharedView: View) {
        val transitionName = ViewCompat.getTransitionName(sharedView).orEmpty()
        showPostScreen(post, sharedView, transitionName, PostFragment.ShowType.COMMENT)
    }

    override fun showCreatePostScreen() {
        showFragment(CreatePostFragment.newInstance(), true)
    }

    override fun showMainScreen() {
        navigation.selectedItemId = R.id.navigation_news
    }

    override fun showAuthScreen() {
        VK.login(this, arrayListOf(VKScope.WALL, VKScope.PHOTOS, VKScope.FRIENDS))
    }

    override fun onBackPressed() {
        if (
            navigation.selectedItemId != R.id.navigation_news
            && supportFragmentManager.backStackEntryCount == 0
        )
            showMainScreen()
        else
            super.onBackPressed()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val callback = object : VKAuthCallback {
            override fun onLogin(token: VKAccessToken) {
                presenter.input.accept(Action.SaveUserToken(token.accessToken))
                (supportFragmentManager.fragments.lastOrNull() as? AuthListener)?.authSuccess()
            }

            override fun onLoginFailed(errorCode: Int) = Unit
        }
        if (data == null || !VK.onActivityResult(requestCode, resultCode, data, callback)) {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun render(state: State) {
        navigation.menu.findItem(R.id.navigation_favorites).isVisible = state.hasFavoritePosts
    }

    private fun showNewsFragment(): Boolean =
        showFragment(
            newsFragment,
            transitionApplier = ::applyHomeScreenAnimation
        )

    private fun showFavoritePostsFragment(): Boolean =
        showFragment(favoritePostsFragment) {
            applyFavoriteScreenAnimation(this, findVisibleFragment())
        }

    private fun showUserProfileFragment(): Boolean =
        showFragment(
            userProfileFragment,
            transitionApplier = ::applyUserProfileScreenAnimation
        )

    private fun showPostScreen(
        post: Post,
        sharedView: View,
        transitionName: String,
        showType: PostFragment.ShowType
    ) {
        showFragment(
            PostFragment.newInstance(post, transitionName, showType),
            true
        ) {
            addSharedElement(sharedView, transitionName)
        }
    }

    private fun showFragment(
        fragment: Fragment,
        addToBackStack: Boolean = false,
        transitionApplier: (FragmentTransaction.() -> Unit)? = null
    ): Boolean {
        if (fragment.transitionIsNotReady()) return false
        supportFragmentManager.beginTransaction().apply {
            transitionApplier?.invoke(this)
            replace(
                R.id.fragmentContainer,
                fragment,
                fragment::class.java.canonicalName
            )
            if (addToBackStack)
                addToBackStack(fragment::class.java.canonicalName)
        }.commit()
        return true
    }

    private fun Fragment.transitionIsNotReady() =
        this.lifecycle.currentState != Lifecycle.State.INITIALIZED

    private fun findVisibleFragment(): Fragment? =
        supportFragmentManager.findFragmentById(R.id.fragmentContainer)?.takeIf { it.isVisible }

    @Suppress("UNCHECKED_CAST")
    private fun <T : Fragment> findFragment(clazz: KClass<out Fragment>): T? =
        supportFragmentManager.findFragmentByTag(clazz.java.canonicalName) as? T
}