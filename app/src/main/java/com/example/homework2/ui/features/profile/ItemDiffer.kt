package com.example.homework2.ui.features.profile

import android.annotation.SuppressLint
import androidx.recyclerview.widget.DiffUtil
import com.example.homework2.model.Post
import com.example.homework2.model.User
import com.example.homework2.ui.screen.RecyclerItem

object ItemDiffer : DiffUtil.ItemCallback<RecyclerItem>() {
    override fun areItemsTheSame(oldItem: RecyclerItem, newItem: RecyclerItem): Boolean =
        when {
            oldItem is Post && newItem is Post -> oldItem.id == newItem.id
            oldItem is User && newItem is User -> oldItem.userId == newItem.userId
            else -> false
        }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: RecyclerItem, newItem: RecyclerItem): Boolean =
        when {
            oldItem is Post && newItem is Post -> oldItem == newItem
            oldItem is User && newItem is User -> oldItem == newItem
            else -> false
        }
}