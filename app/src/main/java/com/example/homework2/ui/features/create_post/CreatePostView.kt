package com.example.homework2.ui.features.create_post

interface CreatePostView {

    fun render(state: State)

    fun handleUiEffect(uiEffect: UiEffect)
}