package com.example.homework2.ui

object TransitionParams {
    private const val SHARED_POST = "shared_post"

    fun postTransitionName(position: Int): String = "$SHARED_POST$position"
}