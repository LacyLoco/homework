package com.example.homework2.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import com.example.homework2.R
import com.facebook.shimmer.ShimmerFrameLayout

class LoadingPostShimmerView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : ShimmerFrameLayout(context, attrs) {

    init {
        LayoutInflater.from(context).inflate(R.layout.post_shimmer_layout, this, true)
    }
}