package com.example.homework2.ui.features.post_details

import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ProgressBar
import androidx.recyclerview.widget.ListAdapter
import com.example.homework2.extension.dp
import com.example.homework2.model.Comment
import com.example.homework2.model.Post
import com.example.homework2.model.ui.Loader
import com.example.homework2.model.ui.Retry
import com.example.homework2.ui.features.base.holders.*
import com.example.homework2.ui.screen.RecyclerItem
import com.example.homework2.ui.view.CommentView
import com.example.homework2.ui.view.PostView
import com.example.homework2.ui.view.PostWithImagesView

class PostDetailAdapter(
    private val repostClick: (Post) -> Unit,
    private val saveImageClick: (Post) -> Unit,
    private val retryClick: () -> Unit
) : ListAdapter<RecyclerItem, BaseHolder<*>>(ItemDiffer) {

    override fun getItemViewType(position: Int): Int =
        when (val item = currentList[position]) {
            is Post -> getPostType(item)
            is Comment -> COMMENT_VIEW
            is Loader -> LOADER_VIEW
            is Retry -> RETRY_VIEW
            else -> throw IllegalArgumentException("undefined view type")
        }

    private fun getPostType(post: Post) =
        when (post.image) {
            null -> POST_VIEW
            else -> POST_IMAGE_VIEW
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseHolder<*> =
        when (viewType) {
            POST_VIEW -> initTextViewHolder(parent)
            POST_IMAGE_VIEW -> initTextWithImageViewHolder(parent)
            COMMENT_VIEW -> initCommentViewHolder(parent)
            LOADER_VIEW -> initLoaderViewHolder(parent)
            RETRY_VIEW -> RetryViewHolder.create(parent.context, retryClick)
            else -> throw IllegalArgumentException("unresolved viewType: $viewType")
        }

    override fun onBindViewHolder(holder: BaseHolder<*>, position: Int) {
        holder._item = currentList[position]
        when (holder) {
            is TextViewHolder -> holder.bind(currentList[position] as Post)
            is TextWithImageViewHolder -> holder.bind(currentList[position] as Post)
            is CommentViewHolder -> holder.bind(currentList[position] as Comment)
        }
    }

    private fun initTextViewHolder(parent: ViewGroup): BaseHolder<*> =
        TextViewHolder(
            PostView(parent.context),
            { _, _ -> },
            { _, _ -> }
        )

    private fun initTextWithImageViewHolder(parent: ViewGroup) =
        TextWithImageViewHolder(
            PostWithImagesView(parent.context),
            { _, _ -> },
            { _, _ -> },
            repostClick,
            saveImageClick
        )

    private fun initCommentViewHolder(parent: ViewGroup) =
        CommentViewHolder(CommentView(parent.context))

    private fun initLoaderViewHolder(parent: ViewGroup) =
        LoaderViewHolder(
            ProgressBar(parent.context).apply {
                layoutParams = FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.MATCH_PARENT,
                    32.dp()
                )
            }
        )

    companion object {
        const val POST_VIEW = 0
        const val POST_IMAGE_VIEW = 1
        const val COMMENT_VIEW = 2
        const val LOADER_VIEW = 3
        const val RETRY_VIEW = 4
    }
}