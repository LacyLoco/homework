package com.example.homework2.ui.features.main

data class State(
    val hasFavoritePosts: Boolean = false
)

internal fun State.reduce(action: Action): State =
    when (action) {
        is Action.HasFavoritePosts -> copy(hasFavoritePosts = action.hasPosts)
        else -> this
    }