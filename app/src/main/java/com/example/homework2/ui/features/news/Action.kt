package com.example.homework2.ui.features.news

import com.example.homework2.model.Post

sealed class Action {
    //UI actions
    object LoadFirstPosts : Action()
    object LoadNextPosts : Action()
    object UpdatePosts : Action()
    object RetryLoadPosts : Action()
    class LikePost(val post: Post) : Action()
    class DislikePost(val post: Post) : Action()
    class RemovePost(val position: Int) : Action()

    //Presenter actions
    data class PostsLoaded(val posts: List<Post>) : Action()
    object StartLoadingNextPosts : Action()
    object EndPostsLoaded : Action()
    data class Error(val error: Throwable) : Action()
    object ErrorLoadingNextPosts : Action()
    object Empty : Action()
}