package com.example.homework2.ui.features.create_post

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import com.example.homework2.R
import com.example.homework2.extension.*
import com.example.homework2.ui.base.BaseFragment
import com.example.homework2.ui.helpers.convertToString
import com.example.homework2.ui.screen.FullScreen
import kotlinx.android.synthetic.main.fragment_create_post.*
import javax.inject.Inject

class CreatePostFragment : BaseFragment<CreatePostView, CreatePostPresenter>(), CreatePostView,
    FullScreen {

    @Inject
    override lateinit var presenter: CreatePostPresenter

    override fun injectComponent() =
        injector.createPostComponent().inject(this)

    override fun releaseComponent() =
        injector.releaseCreatePostComponent()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_create_post, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        focusToTextInput()

        cancel.setOnClickListener {
            requireActivity().onBackPressed()
        }

        btnSend.setOnClickListener {
            publishPost(inputText.text.toString())
            inputText.text.clear()
            inputText.hideKeyboard()
        }

        inputText.afterTextChanged(::toggleButton)
        toggleButton(inputText.text.toString())
    }

    override fun render(state: State) {
        loading.isVisible = state.loading
    }

    override fun handleUiEffect(uiEffect: UiEffect) {
        when (uiEffect) {
            is UiEffect.ShowToast -> showSuccessfulToast()
            is UiEffect.Error -> requireContext().errorDialog(
                uiEffect.error.convertToString(
                    requireContext()
                )
            )
        }
    }

    private fun focusToTextInput() {
        inputText.requestFocus()
        inputText.showKeyboard()
    }

    private fun publishPost(text: String) {
        presenter.input.accept(Action.AddPost(text))
    }

    private fun toggleButton(text: String) {
        btnSend.isEnabled = text.isEmpty().not()
    }

    private fun showSuccessfulToast() {
        Toast.makeText(context, getString(R.string.successPostAdded), Toast.LENGTH_SHORT).show()
    }

    companion object {
        fun newInstance(): CreatePostFragment = CreatePostFragment()
    }
}