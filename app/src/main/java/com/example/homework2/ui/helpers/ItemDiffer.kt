package com.example.homework2.ui.helpers

import android.annotation.SuppressLint
import androidx.recyclerview.widget.DiffUtil
import com.example.homework2.model.Post
import com.example.homework2.model.ui.Error
import com.example.homework2.model.ui.Loader
import com.example.homework2.ui.screen.RecyclerItem

object ItemDiffer : DiffUtil.ItemCallback<RecyclerItem>() {
    override fun areItemsTheSame(oldItem: RecyclerItem, newItem: RecyclerItem): Boolean =
        when {
            oldItem is Post && newItem is Post -> oldItem.id == newItem.id
            oldItem is Error && newItem is Error ->
                oldItem.t.localizedMessage == newItem.t.localizedMessage
            oldItem is Loader && newItem is Loader -> true
            else -> false
        }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: RecyclerItem, newItem: RecyclerItem): Boolean =
        when {
            oldItem is Post && newItem is Post -> oldItem == newItem
            oldItem is Error && newItem is Error -> oldItem == newItem
            else -> false
        }

    override fun getChangePayload(oldItem: RecyclerItem, newItem: RecyclerItem): Any? =
        when {
            oldItem is Post
                    && newItem is Post
                    && oldItem.likes.count != newItem.likes.count -> newItem.likes
            else -> null
        }
}