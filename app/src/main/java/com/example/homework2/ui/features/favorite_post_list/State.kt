package com.example.homework2.ui.features.favorite_post_list

import com.example.homework2.model.Post

data class State(
    val posts: List<Post> = emptyList()
)

internal fun State.reduce(action: Action): State =
    when (action) {
        is Action.FavoritePostsLoaded -> copy(
            posts = action.posts
        )
        else -> this
    }