package com.example.homework2.ui.features.favorite_post_list

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.example.homework2.model.Post
import com.example.homework2.ui.TransitionParams
import com.example.homework2.ui.features.base.holders.BaseHolder
import com.example.homework2.ui.features.base.holders.BasePostViewHolder
import com.example.homework2.ui.features.base.holders.TextViewHolder
import com.example.homework2.ui.features.base.holders.TextWithImageViewHolder
import com.example.homework2.ui.helpers.ItemDiffer
import com.example.homework2.ui.screen.RecyclerItem
import com.example.homework2.ui.view.PostView
import com.example.homework2.ui.view.PostWithImagesView

class FavouritePostsAdapter(
    private val itemClick: (Post, View) -> Unit,
    private val commentClick: (Post, View) -> Unit,
    private val repostClick: (Post) -> Unit,
    private val saveImageClick: (Post) -> Unit,
) : ListAdapter<RecyclerItem, BaseHolder<*>>(ItemDiffer) {

    fun getDecoratorTextForPosition(position: Int): String {
        val currentPost = currentList.getOrNull(position) as? Post
        val previewPost = currentList.getOrNull(position - 1) as? Post

        return if (currentPost?.textDate != previewPost?.textDate) currentPost?.textDate
            ?: "" else ""
    }

    override fun getItemViewType(position: Int): Int =
        when (val item = currentList[position]) {
            is Post -> getPostType(item)
            else -> throw IllegalArgumentException("undefined view type")
        }

    private fun getPostType(post: Post) =
        when (post.image) {
            null -> POST_VIEW
            else -> POST_IMAGE_VIEW
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseHolder<*> =
        when (viewType) {
            POST_VIEW -> initTextViewHolder(parent)
            POST_IMAGE_VIEW -> initTextWithImageViewHolder(parent)
            else -> throw IllegalArgumentException("unresolved viewType: $viewType")
        }

    override fun onBindViewHolder(
        holder: BaseHolder<*>,
        position: Int,
        payloads: MutableList<Any>
    ) {
        payloads.firstOrNull()?.let {
            (holder as? BasePostViewHolder)?.bind((currentList[position] as Post).likes)
        } ?: super.onBindViewHolder(holder, position, payloads)
    }

    override fun onBindViewHolder(holder: BaseHolder<*>, position: Int) {
        holder._item = currentList[position]
        holder.itemView.transitionName = TransitionParams.postTransitionName(position)
        when (holder) {
            is TextViewHolder -> holder.bind(currentList[position] as Post)
            is TextWithImageViewHolder -> holder.bind(currentList[position] as Post)
        }
    }

    private fun initTextViewHolder(parent: ViewGroup) =
        TextViewHolder(PostView(parent.context), commentClick, itemClick)

    private fun initTextWithImageViewHolder(parent: ViewGroup) =
        TextWithImageViewHolder(
            PostWithImagesView(parent.context),
            itemClick,
            commentClick,
            repostClick,
            saveImageClick
        )

    companion object {
        const val POST_VIEW = 0
        const val POST_IMAGE_VIEW = 1
    }
}