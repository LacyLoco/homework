package com.example.homework2.ui.features.news

import com.example.homework2.extension.isNull
import com.example.homework2.model.Post
import com.example.homework2.model.ui.Error
import com.example.homework2.model.ui.FullScreenLoader
import com.example.homework2.model.ui.Loader
import com.example.homework2.model.ui.Retry
import com.example.homework2.ui.screen.RecyclerItem

data class State(
    val items: List<RecyclerItem> = emptyList()
)

internal fun State.reduce(action: Action): State =
    when (action) {
        is Action.LoadFirstPosts ->
            if (this.items.find { it is Post }.isNull())
                copy(
                    items = listOf(FullScreenLoader)
                )
            else this

        is Action.StartLoadingNextPosts ->
            copy(
                items = items.filter { it !is Loader && it !is Retry }
                    .toMutableList()
                    .apply {
                        add(Loader)
                    }
            )
        is Action.UpdatePosts -> copy(
            items = listOf(FullScreenLoader)
        )
        is Action.LikePost -> this
        is Action.DislikePost -> this
        is Action.RemovePost -> this
        is Action.PostsLoaded -> copy(
            items = action.posts,
        )
        is Action.Error -> copy(
            items = listOf(Error(action.error))
        )
        is Action.ErrorLoadingNextPosts -> copy(
            items = items.filter { it !is Loader && it !is Retry }
                .toMutableList()
                .apply {
                    add(Retry)
                }
        )
        else -> this
    }