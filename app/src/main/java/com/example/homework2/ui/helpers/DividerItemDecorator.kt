package com.example.homework2.ui.helpers

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.homework2.extension.dp

class DividerItemDecorator : RecyclerView.ItemDecoration() {

    private val itemSpacing = 16.dp()

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)
        outRect.set(
            0,
            itemSpacing,
            0,
            0
        )
    }

}