package com.example.homework2.ui.features.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.homework2.R
import com.example.homework2.extension.errorDialog
import com.example.homework2.extension.injector
import com.example.homework2.model.Post
import com.example.homework2.ui.base.BaseFragment
import com.example.homework2.ui.helpers.DividerItemDecorator
import com.example.homework2.ui.helpers.addPaginator
import com.example.homework2.ui.helpers.convertToString
import com.example.homework2.ui.router.MainRouter
import com.example.homework2.ui.screen.UserProfileScreen
import kotlinx.android.synthetic.main.fragment_user_profile.*
import javax.inject.Inject

class UserProfileFragment : BaseFragment<UserProfileView, UserProfilePresenter>(), UserProfileView,
    UserProfileScreen {

    @Inject
    override lateinit var presenter: UserProfilePresenter

    override fun injectComponent() =
        injector.userComponent().inject(this)

    private val userProfileAdapter: UserProfileAdapter =
        UserProfileAdapter(
            ::showPostDetailScreen,
            ::commentPost,
            ::shareImage,
            ::showAddToGalleryDialog,
            ::retryClick
        )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_user_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        profileWithPostsList.apply {
            adapter = userProfileAdapter
            addItemDecoration(DividerItemDecorator())
            setHasFixedSize(true)
            addPaginator {
                presenter.input.accept(Action.LoadNextWallPosts)
            }
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    if (dy > 0)
                        floatingActionButton.hide()
                    else
                        floatingActionButton.show()
                    super.onScrolled(recyclerView, dx, dy)
                }
            })
        }

        floatingActionButton.setOnClickListener {
            (requireContext() as? MainRouter)?.showCreatePostScreen()
        }

        userSwipeRefreshLayout.setOnRefreshListener {
            presenter.input.accept(Action.UpdateProfileAndWallPosts)
            userSwipeRefreshLayout.isRefreshing = false
        }

        userProfileAdapter.submitList(presenter.items)
        presenter.input.accept(Action.LoadProfileAndWall)
    }

    override fun render(state: State) {
        userProfileAdapter.submitList(state.items)
    }

    override fun handleUiEffect(uiEffect: UiEffect) {
        when (uiEffect) {
            is UiEffect.Error -> requireContext().errorDialog(
                uiEffect.error.convertToString(
                    requireContext()
                )
            )
        }
    }

    private fun showPostDetailScreen(post: Post, view: View) {
        (requireContext() as? MainRouter)?.showPostDetailsScreen(post, view)
    }

    private fun commentPost(post: Post, view: View) {
        (requireContext() as? MainRouter)?.showPostCommentsScreen(post, view)
    }

    private fun retryClick() {
        presenter.input.accept(Action.RetryLoadProfileAndWall)
    }

    companion object {
        fun newInstance(): UserProfileFragment = UserProfileFragment()
    }
}
