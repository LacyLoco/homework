package com.example.homework2.ui.features.news

interface NewsView {

    fun render(state: State)

    fun handleUiEffect(uiEffect: UiEffect)
}
