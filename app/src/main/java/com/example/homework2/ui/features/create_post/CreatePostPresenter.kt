package com.example.homework2.ui.features.create_post

import com.example.homework2.data.repository.wall.UserWallRepository
import com.example.homework2.di.create_post.CreatePostScope
import com.example.homework2.extension.fromIoThread
import com.example.homework2.ui.base.RxPresenter
import com.freeletics.rxredux.SideEffect
import com.freeletics.rxredux.reduxStore
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import javax.inject.Inject

private typealias CreatePostSideEffect = SideEffect<State, out Action>

@CreatePostScope
class CreatePostPresenter @Inject constructor(
    private val userWallRepository: UserWallRepository
) : RxPresenter<CreatePostView>() {

    private val inputRelay: Relay<Action> = PublishRelay.create()
    private val uiEffectsRelay = PublishRelay.create<UiEffect>()

    val input: Consumer<Action> get() = inputRelay
    private val uiEffectsInput: Observable<UiEffect> get() = uiEffectsRelay

    private val sideEffects = listOf(
        addPost()
    )

    override fun attachView(view: CreatePostView) {
        super.attachView(view)
        inputRelay.reduxStore(
            initialState = State(),
            sideEffects = sideEffects,
            reducer = State::reduce
        ).observeOn(AndroidSchedulers.mainThread())
            .distinctUntilChanged()
            .subscribe(view::render)
            .disposeOnFinish()
        uiEffectsInput.observeOn(AndroidSchedulers.mainThread())
            .subscribe(view::handleUiEffect)
            .disposeOnFinish()
    }

    private fun addPost(): CreatePostSideEffect = { actions, _ ->
        actions.ofType(Action.AddPost::class.java)
            .switchMap {
                addPost(it.message)
            }
    }

    private fun addPost(message: String): Observable<out Action> =
        userWallRepository.addPost(message)
            .toObservable()
            .fromIoThread()
            .map { Action.PostAddedComplete }
            .doOnComplete {
                uiEffectsRelay.accept(UiEffect.ShowToast)
            }
            .onErrorReturn { error ->
                uiEffectsRelay.accept(UiEffect.Error(error))
                Action.PostAddedComplete
            }
}