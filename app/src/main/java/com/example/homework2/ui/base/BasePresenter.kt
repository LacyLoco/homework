package com.example.homework2.ui.base

import androidx.annotation.CallSuper

abstract class BasePresenter<View> : Presenter<View> {

    var view: View? = null

    @CallSuper
    override fun attachView(view: View) {
        this.view = view
    }

    @CallSuper
    override fun detachView(isFinishing: Boolean) {
        view = null
    }
}