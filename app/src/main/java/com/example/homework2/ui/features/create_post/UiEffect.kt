package com.example.homework2.ui.features.create_post

sealed class UiEffect {
    object ShowToast : UiEffect()
    class Error(val error: Throwable) : UiEffect()
}