package com.example.homework2.ui.features.profile

interface UserProfileView {

    fun render(state: State)

    fun handleUiEffect(uiEffect: UiEffect)
}