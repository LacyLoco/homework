package com.example.homework2.ui.features.post_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.transition.TransitionInflater
import com.example.homework2.R
import com.example.homework2.extension.afterTextChanged
import com.example.homework2.extension.errorDialog
import com.example.homework2.extension.injector
import com.example.homework2.extension.showKeyboard
import com.example.homework2.model.Post
import com.example.homework2.ui.base.BaseFragment
import com.example.homework2.ui.helpers.addPaginator
import com.example.homework2.ui.helpers.convertToString
import com.example.homework2.ui.screen.FullScreen
import kotlinx.android.synthetic.main.fragment_post.*
import javax.inject.Inject

class PostFragment : BaseFragment<PostDetailView, PostPresenter>(), PostDetailView, FullScreen {

    @Inject
    override lateinit var presenter: PostPresenter

    private val postDetailAdapter: PostDetailAdapter =
        PostDetailAdapter(::shareImage, ::showAddToGalleryDialog, ::retryClick)

    val post by lazy {
        requireArguments().getParcelable<Post>(ARG_POST)
            ?: throw IllegalArgumentException("undefined post item")
    }

    override fun injectComponent() =
        injector.postComponent().inject(this)

    override fun releaseComponent() =
        injector.releasePostComponent()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.post = post
        postponeEnterTransition()
        sharedElementEnterTransition =
            TransitionInflater.from(context).inflateTransition(android.R.transition.move)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_post, container, false) as ViewGroup

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        postWithCommentsList.apply {
            adapter = postDetailAdapter
            transitionName = requireArguments().getString(TRANSITION_NAME).orEmpty()
            setHasFixedSize(true)
            addPaginator { presenter.input.accept(Action.LoadNextComments) }
        }
        postDetailAdapter.submitList(listOf(post))

        comment_send.setOnClickListener {
            addComment(comment_message.text.toString())
            comment_message.text.clear()
        }

        comment_message.afterTextChanged(::toggleButton)
        toggleButton(comment_message.text.toString())

        postSwipeRefreshLayout.setOnRefreshListener {
            presenter.input.accept(Action.UpdateComments)
            postSwipeRefreshLayout.isRefreshing = false
        }

        input_group.isVisible = post.comments.can_post

        presenter.input.accept(
            Action.LoadFirstComments(
                ShowType.values()[requireArguments().getInt(SHOW_TYPE)]
            )
        )
    }

    override fun render(state: State) {
        postDetailAdapter.submitList(state.items)
        comment_send.isEnabled = state.sendingMessageEnabled
    }

    override fun handleUiEffect(uiEffect: UiEffect) =
        when (uiEffect) {
            is UiEffect.Error -> requireContext().errorDialog(
                uiEffect.error.convertToString(
                    requireContext()
                )
            )
            is UiEffect.ShowComments -> focusToMessageInput()
        }

    private fun focusToMessageInput() {
        comment_message.requestFocus()
        comment_message.showKeyboard()
    }

    private fun addComment(message: String) {
        presenter.input.accept(Action.AddComment(message))
    }

    private fun toggleButton(text: String) {
        comment_send.isEnabled = text.isEmpty().not()
    }

    private fun retryClick() {
        presenter.input.accept(Action.RetryLoadComments)
    }

    companion object {
        private const val SHOW_TYPE = "show_type"
        private const val ARG_POST = "post"
        private const val TRANSITION_NAME = "transition_name"

        fun newInstance(post: Post, transitionName: String?, showType: ShowType): PostFragment =
            PostFragment().apply {
                arguments = bundleOf(
                    SHOW_TYPE to showType.ordinal,
                    ARG_POST to post,
                    TRANSITION_NAME to transitionName
                )
            }
    }

    enum class ShowType {
        POST, COMMENT
    }
}