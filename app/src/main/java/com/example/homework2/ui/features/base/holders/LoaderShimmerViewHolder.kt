package com.example.homework2.ui.features.base.holders

import android.view.View
import com.example.homework2.model.ui.FullScreenLoader

class LoaderShimmerViewHolder(itemView: View) : BaseHolder<FullScreenLoader>(itemView)