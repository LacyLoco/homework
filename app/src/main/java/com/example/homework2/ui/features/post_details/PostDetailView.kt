package com.example.homework2.ui.features.post_details

interface PostDetailView {

    fun render(state: State)

    fun handleUiEffect(uiEffect: UiEffect)
}