package com.example.homework2.ui.features.news

import com.example.homework2.data.converter.AuthException
import com.example.homework2.data.repository.news.NewsRepository
import com.example.homework2.di.news.NewsScope
import com.example.homework2.extension.fromIoThread
import com.example.homework2.extension.toMainThread
import com.example.homework2.extension.toObservable
import com.example.homework2.model.Post
import com.example.homework2.ui.base.RxPresenter
import com.freeletics.rxredux.SideEffect
import com.freeletics.rxredux.reduxStore
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.reactivex.Observable
import io.reactivex.functions.Consumer
import javax.inject.Inject

private typealias PostsSideEffect = SideEffect<State, out Action>

@NewsScope
class NewsPresenter @Inject constructor(
    private val newsRepository: NewsRepository
) : RxPresenter<NewsView>() {

    private val inputRelay: Relay<Action> = PublishRelay.create()
    private val uiEffectsRelay = PublishRelay.create<UiEffect>()

    val input: Consumer<Action> get() = inputRelay
    private val uiEffectsInput: Observable<UiEffect> get() = uiEffectsRelay

    private val sideEffects = listOf(
        loadFirstPosts(),
        loadNextPosts(),
        updatePosts(),
        likePost(),
        dislikePost(),
        removePost(),
        retryLoadPosts()
    )

    @Volatile
    var posts = listOf<Post>()

    override fun attachView(view: NewsView) {
        super.attachView(view)
        inputRelay.reduxStore(
            initialState = State(items = posts),
            sideEffects = sideEffects,
            reducer = State::reduce
        ).distinctUntilChanged()
            .toMainThread()
            .subscribe(view::render)
            .disposeOnFinish()
        uiEffectsInput
            .toMainThread()
            .subscribe(view::handleUiEffect)
            .disposeOnFinish()
    }

    private fun loadFirstPosts(): PostsSideEffect = { actions, _ ->
        actions.ofType(Action.LoadFirstPosts::class.java)
            .switchMap {
                loadPosts()
            }
    }

    private fun loadNextPosts(): PostsSideEffect = { actions, _ ->
        actions.ofType(Action.LoadNextPosts::class.java)
            .filter { posts.isNotEmpty() }
            .map { posts.size }
            .distinctUntilChanged()
            .switchMap { offset ->
                loadNextPosts(offset, posts.lastOrNull()?.nextFrom)
            }
    }

    private fun updatePosts(): PostsSideEffect = { actions, _ ->
        actions.ofType(Action.UpdatePosts::class.java)
            .switchMap {
                loadAndUpdatePosts()
            }
    }

    private fun likePost(): PostsSideEffect = { actions, _ ->
        actions.ofType(Action.LikePost::class.java)
            .switchMap {
                newsRepository.addLike(it.post)
                    .toObservable()
                    .fromIoThread()
                    .map { Action.Empty }
            }
    }

    private fun dislikePost(): PostsSideEffect = { actions, _ ->
        actions.ofType(Action.DislikePost::class.java)
            .switchMap {
                newsRepository.deleteLike(it.post)
                    .toObservable()
                    .fromIoThread()
                    .map { Action.Empty }
            }
    }

    private fun removePost(): PostsSideEffect = { actions, _ ->
        actions.ofType(Action.RemovePost::class.java)
            .switchMap {
                newsRepository.removePost(posts[it.position])
                    .toObservable()
                    .fromIoThread()
                    .map { Action.Empty }
                    .onErrorReturn { error ->
                        handleError(error)
                        Action.Empty
                    }
            }
    }

    private fun loadPosts(): Observable<Action> =
        newsRepository.getPosts()
            .fromIoThread()
            .doOnNext { posts = it }
            .flatMap {
                if (posts.isEmpty())
                    loadAndUpdatePosts()
                else Action.PostsLoaded(posts = it).toObservable()
            }

    private fun loadAndUpdatePosts(): Observable<Action> =
        newsRepository.updatePosts()
            .fromIoThread()
            .map { Action.PostsLoaded(posts = it) as Action }
            .onErrorReturn { error ->
                handleError(error, false)
                Action.Error(error)
            }

    private fun loadNextPosts(
        newOffset: Int,
        startFrom: String? = null
    ): Observable<Action> =
        newsRepository.getNextPosts(newOffset, startFrom)
            .fromIoThread()
            .map { Action.EndPostsLoaded as Action }
            .startWith(Action.StartLoadingNextPosts)
            .onErrorReturn { error ->
                handleError(error)
                Action.ErrorLoadingNextPosts
            }

    private fun handleError(error: Throwable, needShow: Boolean = true) =
        when {
            error is AuthException -> uiEffectsRelay.accept(UiEffect.UnAuth)
            needShow -> uiEffectsRelay.accept(UiEffect.Error(error))
            else -> Unit
        }

    private fun retryLoadPosts(): PostsSideEffect = { actions, state ->
        actions.ofType(Action.RetryLoadPosts::class.java)
            .switchMap {
                if (posts.isEmpty())
                    loadAndUpdatePosts()
                else
                    loadNextPosts(posts.size, posts.lastOrNull()?.nextFrom)
            }
    }
}