package com.example.homework2.ui.features.main

import com.example.homework2.data.repository.news.NewsRepository
import com.example.homework2.data.repository.storage.UserStorageRepository
import com.example.homework2.extension.fromIoThread
import com.example.homework2.ui.base.RxPresenter
import com.freeletics.rxredux.SideEffect
import com.freeletics.rxredux.reduxStore
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import javax.inject.Inject

private typealias MainSideEffect = SideEffect<State, out Action>

class MainPresenter @Inject constructor(
    private val newsRepository: NewsRepository,
    private val userStorageRepository: UserStorageRepository
) : RxPresenter<MainView>() {

    private val inputRelay: Relay<Action> = PublishRelay.create()

    val input: Consumer<Action> get() = inputRelay

    private val sideEffects = listOf(
        isFavoritePostsExists(),
        saveAccessToken()
    )

    private val state: Observable<State> = inputRelay.reduxStore(
        initialState = State(),
        sideEffects = sideEffects,
        reducer = State::reduce
    )

    override fun attachView(view: MainView) {
        super.attachView(view)
        state.observeOn(AndroidSchedulers.mainThread())
            .distinctUntilChanged()
            .subscribe(view::render)
            .disposeOnFinish()
    }

    private fun isFavoritePostsExists(): MainSideEffect = { actions, _ ->
        actions.ofType(Action.CheckHasFavoritePosts::class.java)
            .switchMap {
                newsRepository.getFavoritePosts()
                    .fromIoThread()
                    .map { Action.HasFavoritePosts(it.isNotEmpty()) }
            }
    }

    private fun saveAccessToken(): MainSideEffect = { actions, _ ->
        actions.ofType(Action.SaveUserToken::class.java)
            .switchMap {
                Observable.fromCallable {
                    userStorageRepository.accessToken = it.accessToken
                }
                    .fromIoThread()
                    .map { Action.Empty }
            }
    }
}