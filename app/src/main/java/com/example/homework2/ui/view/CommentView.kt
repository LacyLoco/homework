package com.example.homework2.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.homework2.R
import com.example.homework2.extension.*
import com.example.homework2.model.Comment
import kotlinx.android.synthetic.main.comment_layout.view.*
import kotlin.math.max

class CommentView @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ViewGroup(context, attributeSet, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.comment_layout, this, true)
    }

    private val avatarImage: ImageView = commentAvatarImage
    private val author: TextView = commentAuthor
    private val text: TextView = commentText
    private val date: TextView = commentDate

    fun bind(comment: Comment) {
        avatarImage.loadCircle(comment.author.avatar)
        author.text = comment.author.name
        text.text = comment.text
        date.text = comment.textDate
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val desiredWidth = MeasureSpec.getSize(widthMeasureSpec)
        var height = 0

        //Header
        measureChildWithMargins(avatarImage, widthMeasureSpec, 0, heightMeasureSpec, height)
        measureChildWithMargins(
            author,
            widthMeasureSpec - avatarImage.getWidthWithMargins(),
            0,
            heightMeasureSpec,
            height
        )
        measureChildWithMargins(
            text,
            widthMeasureSpec - avatarImage.getWidthWithMargins(),
            0,
            heightMeasureSpec,
            height
        )

        measureChildWithMargins(date, widthMeasureSpec, 0, heightMeasureSpec, height)

        height += max(
            avatarImage.getHeightWithMargins(),
            author.getHeightWithMargins() + text.getHeightWithMargins() + date.getHeightWithMargins()
        )

        setMeasuredDimension(desiredWidth, View.resolveSize(height, heightMeasureSpec))
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        val currentLeft = l + paddingLeft
        var currentTop = paddingTop

        //Avatar
        val avatarLeft = avatarImage.calculateViewLeft(currentLeft)
        val avatarTop = avatarImage.calculateViewTop(currentTop)
        val avatarRight = avatarLeft + avatarImage.measuredWidth
        val avatarBottom = avatarTop + avatarImage.measuredHeight

        avatarImage.layout(avatarLeft, avatarTop, avatarRight, avatarBottom)

        //Title
        val authorTextLeft = author.calculateViewLeft(avatarRight)
        val authorTextTop = author.calculateViewTop(currentTop)
        val authorTextBottom = authorTextTop + author.measuredHeight

        author.layout(
            authorTextLeft,
            authorTextTop,
            authorTextLeft + author.measuredWidth,
            authorTextBottom
        )
        currentTop = authorTextBottom

        //Comment
        val commentTextLeft = text.calculateViewLeft(avatarRight)
        val commentTextBottom = currentTop + text.measuredHeight

        text.layout(
            commentTextLeft,
            currentTop,
            commentTextLeft + text.measuredWidth,
            commentTextBottom
        )

        currentTop = max(avatarBottom, commentTextBottom)

        //Date
        val dateTextLeft = date.calculateViewLeft(avatarRight)
        val dateTextBottom = currentTop + date.measuredHeight

        date.layout(
            dateTextLeft,
            currentTop,
            dateTextLeft + date.measuredWidth,
            dateTextBottom
        )
    }

    override fun generateDefaultLayoutParams(): LayoutParams =
        MarginLayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)

    override fun generateLayoutParams(attrs: AttributeSet?): LayoutParams =
        MarginLayoutParams(context, attrs)
}