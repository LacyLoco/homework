package com.example.homework2.ui.features.profile

import com.example.homework2.data.repository.user.UserProfileRepository
import com.example.homework2.data.repository.wall.UserWallRepository
import com.example.homework2.di.user.UserScope
import com.example.homework2.extension.fromIoThread
import com.example.homework2.model.Post
import com.example.homework2.model.User
import com.example.homework2.ui.base.RxPresenter
import com.example.homework2.ui.screen.RecyclerItem
import com.freeletics.rxredux.SideEffect
import com.freeletics.rxredux.reduxStore
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Consumer
import javax.inject.Inject

private typealias ProfileSideEffect = SideEffect<State, out Action>

@UserScope
class UserProfilePresenter @Inject constructor(
    private val userProfileRepository: UserProfileRepository,
    private val userWallRepository: UserWallRepository,
) : RxPresenter<UserProfileView>() {

    private val inputRelay: Relay<Action> = PublishRelay.create()
    private val uiEffectsRelay = PublishRelay.create<UiEffect>()

    val input: Consumer<Action> get() = inputRelay
    private val uiEffectsInput: Observable<UiEffect> get() = uiEffectsRelay

    private val sideEffects = listOf(
        loadProfileAndWallPosts(),
        loadNextWallPosts(),
        updateProfileAndPosts(),
        retryLoadProfileAndWall()
    )

    @Volatile
    var items = listOf<RecyclerItem>()

    override fun attachView(view: UserProfileView) {
        super.attachView(view)
        inputRelay.reduxStore(
            initialState = State(items = items),
            sideEffects = sideEffects,
            reducer = State::reduce
        ).observeOn(AndroidSchedulers.mainThread())
            .distinctUntilChanged()
            .subscribe(view::render)
            .disposeOnFinish()
        uiEffectsInput.observeOn(AndroidSchedulers.mainThread())
            .subscribe(view::handleUiEffect)
            .disposeOnFinish()
    }

    private fun loadProfileAndWallPosts(): ProfileSideEffect = { actions, _ ->
        actions.ofType(Action.LoadProfileAndWall::class.java)
            .switchMap {
                loadProfileAndWall()
            }
    }

    private fun loadProfileAndWall(): Observable<Action> =
        Observable.combineLatest(
            userProfileRepository.getUserProfile(),
            userWallRepository.getUserWallPosts(),
            BiFunction { user: User, wall: List<Post> ->
                mutableListOf<RecyclerItem>(user).apply {
                    addAll(wall)
                }
            }
        )
            .fromIoThread()
            .doOnNext { items = it }
            .map { Action.ProfileAndWallLoaded(items = it) as Action }
            .onErrorReturn { error ->
                uiEffectsRelay.accept(UiEffect.Error(error))
                Action.Error(error)
            }

    private fun loadNextWallPosts(): ProfileSideEffect = { actions, state ->
        actions.ofType(Action.LoadNextWallPosts::class.java)
            .filter {
                state().wallOffset != NO_OFFSET
            }
            .map { state().wallOffset }
            .distinctUntilChanged()
            .switchMap { wallOffset ->
                loadNextPosts(wallOffset)
            }
    }

    private fun loadNextPosts(
        startFrom: Int
    ): Observable<Action> =
        userWallRepository.getNextUserWallPosts(startFrom)
            .toObservable()
            .fromIoThread()
            .map { Action.EndLoadingNextPage as Action }
            .startWith(Action.StartLoadingNextPage)
            .onErrorReturn { error ->
                uiEffectsRelay.accept(UiEffect.Error(error))
                Action.ErrorPostLoading
            }

    private fun updateProfileAndPosts(): ProfileSideEffect = { actions, state ->
        actions.ofType(Action.UpdateProfileAndWallPosts::class.java)
            .switchMap {
                if (state().wallOffset == NO_OFFSET)
                    loadProfileAndWall()
                else
                    loadProfileAndUpdatePosts()
                        .onErrorReturn {
                            Action.Error(it)
                        }
            }
    }

    private fun retryLoadProfileAndWall(): ProfileSideEffect = { actions, state ->
        actions.ofType(Action.RetryLoadProfileAndWall::class.java)
            .switchMap {
                if (state().wallOffset == NO_OFFSET)
                    loadProfileAndUpdatePosts()
                else
                    loadNextPosts(state().wallOffset)
            }
    }

    private fun loadProfileAndUpdatePosts(): Observable<Action> =
        Single.zip(
            userProfileRepository.updateUserProfile(),
            userWallRepository.updatePosts(),
            zipper
        )
            .toObservable()
            .fromIoThread()
            .map { Action.EndLoadingNextPage as Action }
            .onErrorReturn {
                Action.Error(it)
            }

    private val zipper = BiFunction { user: User, wall: List<Post> ->
        mutableListOf<RecyclerItem>(user).apply {
            addAll(wall)
        }
    }
}