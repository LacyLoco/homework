package com.example.homework2.ui.features.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.example.homework2.R
import com.example.homework2.ui.screen.HomeScreen

object RouterAnimationHelper {

    fun applyHomeScreenAnimation(transaction: FragmentTransaction) {
        transaction.setCustomAnimations(
            R.anim.enter_from_left,
            R.anim.exit_to_right,
            R.anim.enter_from_right,
            R.anim.exit_to_left
        )
    }

    fun applyFavoriteScreenAnimation(transaction: FragmentTransaction, visibleFragment: Fragment?) {
        if (visibleFragment is HomeScreen)
            transaction.setCustomAnimations(
                R.anim.enter_from_right,
                R.anim.exit_to_left,
                R.anim.enter_from_left,
                R.anim.exit_to_right
            )
        else
            transaction.setCustomAnimations(
                R.anim.enter_from_left,
                R.anim.exit_to_right,
                R.anim.enter_from_right,
                R.anim.exit_to_left
            )
    }

    fun applyUserProfileScreenAnimation(transaction: FragmentTransaction) {
        transaction.setCustomAnimations(
            R.anim.enter_from_right,
            R.anim.exit_to_left,
            R.anim.enter_from_left,
            R.anim.exit_to_right
        )
    }
}