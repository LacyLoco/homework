package com.example.homework2.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import com.example.homework2.R
import com.example.homework2.extension.*
import com.example.homework2.model.User
import kotlinx.android.synthetic.main.profile_layout.view.*
import kotlin.math.max

class UserProfileView @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ViewGroup(context, attributeSet, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.profile_layout, this, true)
    }

    private val avatarImage: ImageView = profileAvatarImage
    private val userId: TextView = userProfileId
    private val fullName: TextView = userFullName
    private val lastSeen: TextView = userLastSeen
    private val country: TextView = userCountry
    private val city: TextView = userCity
    private val bDate: TextView = userBDate
    private val career: TextView = userCareer
    private val education: TextView = userEducation
    private val followers: TextView = userFollowers
    private val aboutMe: TextView = userAboutMe

    fun bind(user: User) {
        user.firstName
        avatarImage.loadCircle(user.photo)
        userId.text = user.domain ?: user.userId.toString()
        fullName.text = user.fullName
        lastSeen.text = user.lastSeenText

        country.text = context.getString(R.string.profileCountry, user.country?.title)
        country.isVisible = user.country?.title.isNotNull()

        city.text = context.getString(R.string.profileCity, user.city?.title)
        city.isVisible = user.city?.title.isNotNull()

        bDate.text = context.getString(R.string.profileBDate, user.bDate)
        bDate.isVisible = user.bDate.isNotNull()

        career.text = context.getString(R.string.profileCareer, user.career?.position)
        career.isVisible = user.career?.position.isNotNull()

        education.text = context.getString(R.string.profileEducation, user.education)
        education.isVisible = user.education.isNullOrEmpty().not()

        followers.text = resources.getQuantityString(
            R.plurals.followers,
            user.followersCount,
            user.followersCount
        )

        aboutMe.text = context.getString(R.string.profileAboutMe, user.about)
        aboutMe.isVisible = user.about.isNullOrEmpty().not()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val desiredWidth = MeasureSpec.getSize(widthMeasureSpec)
        var height = 0

        measureChildWithMargins(avatarImage, widthMeasureSpec, 0, heightMeasureSpec, height)
        measureChildWithMargins(
            userId,
            widthMeasureSpec - avatarImage.getWidthWithMargins(),
            0,
            heightMeasureSpec,
            height
        )
        measureChildWithMargins(
            fullName,
            widthMeasureSpec - avatarImage.getWidthWithMargins(),
            0,
            heightMeasureSpec,
            height
        )

        measureChildWithMargins(
            lastSeen,
            widthMeasureSpec - avatarImage.getWidthWithMargins(),
            0,
            heightMeasureSpec,
            height
        )

        height += max(
            avatarImage.getHeightWithMargins(),
            userId.getHeightWithMargins() +
                    fullName.getHeightWithMargins() +
                    lastSeen.getHeightWithMargins()
        )

        if (country.isVisible)
            measureChildWithMargins(country, widthMeasureSpec, 0, heightMeasureSpec, height)

        height += country.getHeightWithMargins()

        if (city.isVisible)
            measureChildWithMargins(city, widthMeasureSpec, 0, heightMeasureSpec, height)

        height += city.getHeightWithMargins()

        if (bDate.isVisible)
            measureChildWithMargins(bDate, widthMeasureSpec, 0, heightMeasureSpec, height)

        height += bDate.getHeightWithMargins()

        if (career.isVisible)
            measureChildWithMargins(career, widthMeasureSpec, 0, heightMeasureSpec, height)

        height += career.getHeightWithMargins()

        if (education.isVisible)
            measureChildWithMargins(education, widthMeasureSpec, 0, heightMeasureSpec, height)

        height += education.getHeightWithMargins()

        if (followers.isVisible)
            measureChildWithMargins(followers, widthMeasureSpec, 0, heightMeasureSpec, height)

        height += followers.getHeightWithMargins()

        if (aboutMe.isVisible)
            measureChildWithMargins(aboutMe, widthMeasureSpec, 0, heightMeasureSpec, height)

        height += aboutMe.getHeightWithMargins()

        setMeasuredDimension(desiredWidth, View.resolveSize(height, heightMeasureSpec))
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        val currentLeft = l + paddingLeft
        var currentTop = paddingTop

        //Avatar
        val avatarLeft = avatarImage.calculateViewLeft(currentLeft)
        val avatarTop = avatarImage.calculateViewTop(currentTop)
        val avatarRight = avatarLeft + avatarImage.measuredWidth
        val avatarBottom = avatarTop + avatarImage.measuredHeight

        avatarImage.layout(avatarLeft, avatarTop, avatarRight, avatarBottom)

        //UserId or domain
        val userIdTextLeft = userId.calculateViewLeft(avatarRight)
        val userIdTextTop = userId.calculateViewTop(currentTop)
        val userIdTextBottom = userIdTextTop + userId.measuredHeight

        userId.layout(
            userIdTextLeft,
            userIdTextTop,
            userIdTextLeft + userId.measuredWidth,
            userIdTextBottom
        )
        currentTop = userIdTextBottom

        //First name and Last name
        val fullNameTextLeft = fullName.calculateViewLeft(avatarRight)
        val fullNameTextBottom = currentTop + fullName.measuredHeight

        fullName.layout(
            fullNameTextLeft,
            currentTop,
            fullNameTextLeft + fullName.measuredWidth,
            fullNameTextBottom
        )

        currentTop = fullNameTextBottom

        //last seen
        val lastSeenTextLeft = lastSeen.calculateViewLeft(avatarRight)
        val lastSeenTextBottom = currentTop + lastSeen.measuredHeight

        lastSeen.layout(
            lastSeenTextLeft,
            currentTop,
            lastSeenTextLeft + lastSeen.measuredWidth,
            lastSeenTextBottom
        )

        currentTop = max(avatarBottom, lastSeenTextBottom)

        //country
        if (country.isVisible) {
            val countryTextLeft = country.calculateViewLeft(currentLeft)
            val countryTextTop = country.calculateViewTop(currentTop)
            val countryTextBottom = countryTextTop + country.measuredHeight

            country.layout(
                countryTextLeft,
                countryTextTop,
                countryTextLeft + country.measuredWidth,
                countryTextBottom
            )

            currentTop = countryTextBottom
        }

        //city
        if (city.isVisible) {
            val cityTextLeft = city.calculateViewLeft(currentLeft)
            val cityTextTop = city.calculateViewTop(currentTop)
            val cityTextBottom = cityTextTop + city.measuredHeight

            city.layout(
                cityTextLeft,
                cityTextTop,
                cityTextLeft + city.measuredWidth,
                cityTextBottom
            )

            currentTop = cityTextBottom
        }

        //bdate
        if (bDate.isVisible) {
            val bDateTextLeft = bDate.calculateViewLeft(currentLeft)
            val bDateTextTop = bDate.calculateViewTop(currentTop)
            val bDateTextBottom = bDateTextTop + bDate.measuredHeight

            bDate.layout(
                bDateTextLeft,
                bDateTextTop,
                bDateTextLeft + bDate.measuredWidth,
                bDateTextBottom
            )

            currentTop = bDateTextBottom
        }

        //career
        if (career.isVisible) {
            val careerTextLeft = career.calculateViewLeft(currentLeft)
            val careerTextTop = career.calculateViewTop(currentTop)
            val careerTextBottom = careerTextTop + career.measuredHeight

            career.layout(
                careerTextLeft,
                careerTextTop,
                careerTextLeft + career.measuredWidth,
                careerTextBottom
            )

            currentTop = careerTextBottom
        }

        //education
        if (education.isVisible) {
            val educationTextLeft = education.calculateViewLeft(currentLeft)
            val educationTextTop = education.calculateViewTop(currentTop)
            val educationTextBottom = educationTextTop + education.measuredHeight

            education.layout(
                educationTextLeft,
                educationTextTop,
                educationTextLeft + education.measuredWidth,
                educationTextBottom
            )

            currentTop = educationTextBottom
        }

        //followers
        if (followers.isVisible) {
            val followersTextLeft = followers.calculateViewLeft(currentLeft)
            val followersTextTop = followers.calculateViewTop(currentTop)
            val followersTextBottom = followersTextTop + followers.measuredHeight

            followers.layout(
                followersTextLeft,
                followersTextTop,
                followersTextLeft + followers.measuredWidth,
                followersTextBottom
            )

            currentTop = followersTextBottom
        }

        //aboutMe
        if (aboutMe.isVisible) {
            val aboutMeTextLeft = aboutMe.calculateViewLeft(currentLeft)
            val aboutMeTextTop = aboutMe.calculateViewTop(currentTop)
            val aboutMeTextBottom = aboutMeTextTop + aboutMe.measuredHeight

            aboutMe.layout(
                aboutMeTextLeft,
                aboutMeTextTop,
                aboutMeTextLeft + aboutMe.measuredWidth,
                aboutMeTextBottom
            )
        }
    }

    override fun generateDefaultLayoutParams(): LayoutParams =
        MarginLayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)

    override fun generateLayoutParams(attrs: AttributeSet?): LayoutParams =
        MarginLayoutParams(context, attrs)
}