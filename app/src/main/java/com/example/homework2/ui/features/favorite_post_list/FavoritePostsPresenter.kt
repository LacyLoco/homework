package com.example.homework2.ui.features.favorite_post_list

import com.example.homework2.data.repository.news.NewsRepository
import com.example.homework2.di.favorite_posts.FavoritePostsScope
import com.example.homework2.extension.fromIoThread
import com.example.homework2.ui.base.RxPresenter
import com.freeletics.rxredux.SideEffect
import com.freeletics.rxredux.reduxStore
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import javax.inject.Inject

private typealias FavoritePostsSideEffect = SideEffect<State, out Action>

@FavoritePostsScope
class FavoritePostsPresenter @Inject constructor(
    private val newsRepository: NewsRepository
) : RxPresenter<FavoritePostsView>() {

    private val inputRelay: Relay<Action> = PublishRelay.create()
    private val uiEffectsRelay = PublishRelay.create<UiEffect>()

    val input: Consumer<Action> get() = inputRelay
    private val uiEffectsInput: Observable<UiEffect> get() = uiEffectsRelay

    private val sideEffects = listOf(
        loadFavoritePosts(),
        dislikePost()
    )

    private val state: Observable<State> = inputRelay.reduxStore(
        initialState = State(),
        sideEffects = sideEffects,
        reducer = State::reduce
    )

    override fun attachView(view: FavoritePostsView) {
        super.attachView(view)
        state.observeOn(AndroidSchedulers.mainThread())
            .distinctUntilChanged()
            .subscribe(view::render)
            .disposeOnFinish()
        uiEffectsInput.observeOn(AndroidSchedulers.mainThread())
            .subscribe(view::handleUiEffect)
            .disposeOnFinish()
    }

    private fun loadFavoritePosts(): FavoritePostsSideEffect = { actions, _ ->
        actions.ofType(Action.LoadFavoritePosts::class.java)
            .switchMap {
                newsRepository.getFavoritePosts()
                    .fromIoThread()
                    .doOnNext {
                        if (it.isEmpty())
                            uiEffectsRelay.accept(UiEffect.PostsIsEmpty)
                    }
                    .fromIoThread()
                    .map { Action.FavoritePostsLoaded(posts = it) }
            }
    }

    private fun dislikePost(): FavoritePostsSideEffect = { actions, _ ->
        actions.ofType(Action.DislikePost::class.java)
            .switchMap {
                newsRepository.deleteLike(it.post)
                    .toObservable()
                    .fromIoThread()
                    .map { Action.Empty }
            }
    }
}
