package com.example.homework2.ui.helpers

import android.annotation.SuppressLint
import android.graphics.Canvas
import android.view.MotionEvent
import android.view.View
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.ItemTouchHelper.ACTION_STATE_SWIPE
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.homework2.extension.dpf
import kotlin.math.abs
import kotlin.math.min

class SwipeHelper(
    private val itemRemoved: (Int) -> Unit,
    private val itemLiked: (Int) -> Unit
) : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.START or ItemTouchHelper.END) {

    private val MAX_RIGHT_SWIPE_LENGTH = 150.dpf()
    private val ANIMATION_DURATION = 150L

    private var swipeBack = false

    override fun onMove(rv: RecyclerView, vh: ViewHolder, target: ViewHolder): Boolean = false

    override fun clearView(recyclerView: RecyclerView, viewHolder: ViewHolder) =
        getDefaultUIUtil().clearView(viewHolder.itemView)

    override fun onSelectedChanged(viewHolder: ViewHolder?, actionState: Int) {
        if (viewHolder != null)
            getDefaultUIUtil().onSelected(viewHolder.itemView)
    }

    override fun onSwiped(viewHolder: ViewHolder, direction: Int) {
        if (direction == ItemTouchHelper.START)
            itemRemoved.invoke(viewHolder.adapterPosition)
    }

    override fun convertToAbsoluteDirection(flags: Int, layoutDirection: Int): Int =
        if (swipeBack) {
            swipeBack = false
            0
        } else super.convertToAbsoluteDirection(flags, layoutDirection)

    override fun onChildDraw(
        c: Canvas,
        rv: RecyclerView,
        vh: ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        if (actionState != ACTION_STATE_SWIPE) return

        val newDx = if (dX > 0) min(dX, MAX_RIGHT_SWIPE_LENGTH) else dX

        if (dX > 0) {
            vh.itemView.alpha = 1f
            toggleTouch(rv, vh, newDx, toAdd = true)
        } else {
            vh.itemView.alpha = 1 - (abs(dX) / rv.width)
            toggleTouch(rv, vh, newDx, toAdd = false)
        }

        getDefaultUIUtil().onDraw(c, rv, vh.itemView, newDx, dY, actionState, isCurrentlyActive)
    }

    @SuppressLint("ClickableViewAccessibility")
    fun toggleTouch(recyclerView: RecyclerView, vh: ViewHolder, dX: Float, toAdd: Boolean) {
        if (toAdd)
            recyclerView.setOnTouchListener { view, event ->
                swipeBack =
                    event.action == MotionEvent.ACTION_CANCEL
                            || event.action == MotionEvent.ACTION_UP
                if (swipeBack && dX == MAX_RIGHT_SWIPE_LENGTH)
                    view.invokeAfterAnimation { itemLiked.invoke(vh.adapterPosition) }
                false
            }
        else recyclerView.setOnTouchListener(null)
    }

    private fun View.invokeAfterAnimation(action: () -> Unit) =
        postDelayed(action, ANIMATION_DURATION)
}