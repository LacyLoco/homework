package com.example.homework2.ui.helpers

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.text.TextPaint
import android.view.View
import androidx.core.view.forEach
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.NO_POSITION
import com.example.homework2.R
import com.example.homework2.extension.dp
import com.example.homework2.extension.dpf

class DividerTextItemDecoration(
    context: Context,
    private val textForPosition: (Int) -> String
) : RecyclerView.ItemDecoration() {

    private val textSize = 14.dpf()
    private val itemSpacing = 16.dp()
    private val itemTextSpacing = 36.dp()
    private val paint: Paint = TextPaint().apply {
        color = context.getColor(R.color.textColor)
        textSize = this@DividerTextItemDecoration.textSize
    }
    private val bounds = Rect()

    override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        super.onDrawOver(c, parent, state)
        parent.forEach loop@{ view ->

            val position = parent.getChildAdapterPosition(view)

            if (position == NO_POSITION) return@loop

            val dateText = textForPosition.invoke(position)

            if (dateText.isEmpty()) return@loop

            paint.getTextBounds(dateText, 0, dateText.length, bounds)

            c.drawText(
                dateText,
                (view.right / 2 - bounds.width() / 2).toFloat(),
                view.top.toFloat() - itemTextSpacing / 2 + textSize / 3,
                paint
            )
        }
    }

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)
        val adapterPosition = parent.getChildAdapterPosition(view)
        outRect.set(
            0,
            getItemSpacing(adapterPosition),
            0,
            0
        )
    }

    private fun getItemSpacing(position: Int): Int =
        if (textForPosition.invoke(position).isNotEmpty())
            itemTextSpacing
        else
            itemSpacing
}