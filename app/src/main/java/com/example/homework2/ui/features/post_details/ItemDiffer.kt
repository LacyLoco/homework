package com.example.homework2.ui.features.post_details

import android.annotation.SuppressLint
import androidx.recyclerview.widget.DiffUtil
import com.example.homework2.model.Comment
import com.example.homework2.model.Post
import com.example.homework2.ui.screen.RecyclerItem

object ItemDiffer : DiffUtil.ItemCallback<RecyclerItem>() {
    override fun areItemsTheSame(oldItem: RecyclerItem, newItem: RecyclerItem): Boolean =
        when {
            oldItem is Post && newItem is Post -> oldItem.id == newItem.id
            oldItem is Comment && newItem is Comment -> oldItem.id == newItem.id
            else -> false
        }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: RecyclerItem, newItem: RecyclerItem): Boolean =
        when {
            oldItem is Post && newItem is Post -> oldItem == newItem
            oldItem is Comment && newItem is Comment -> oldItem == newItem
            else -> false
        }
}