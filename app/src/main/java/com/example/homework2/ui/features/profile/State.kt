package com.example.homework2.ui.features.profile

import com.example.homework2.extension.isNull
import com.example.homework2.model.Post
import com.example.homework2.model.User
import com.example.homework2.model.ui.Error
import com.example.homework2.model.ui.FullScreenLoader
import com.example.homework2.model.ui.Loader
import com.example.homework2.model.ui.Retry
import com.example.homework2.ui.screen.RecyclerItem

const val NO_OFFSET = -1

data class State(
    val wallOffset: Int = NO_OFFSET,
    val items: List<RecyclerItem> = emptyList()
)


internal fun State.reduce(action: Action): State =
    when (action) {
        is Action.LoadProfileAndWall ->
            if (this.items.find { it is User }.isNull())
                copy(
                    items = listOf(FullScreenLoader)
                )
            else this
        is Action.UpdateProfileAndWallPosts -> copy(
            items = listOf(FullScreenLoader)
        )
        is Action.StartLoadingNextPage ->
            copy(
                items = items.toMutableList().apply {
                    add(Loader)
                }
            )
        is Action.EndLoadingNextPage ->
            copy(
                items = items.filter { it !is Loader && it !is Retry }
            )
        is Action.ProfileAndWallLoaded -> copy(
            wallOffset = action.items.indexOfLast { it is Post },
            items = action.items
        )
        is Action.Error -> copy(
            items = listOf(Error(action.error))
        )
        is Action.ErrorPostLoading -> copy(
            items = items.filter { it !is Loader && it !is Retry }
                .toMutableList()
                .apply {
                    add(Retry)
                }
        )
        else -> this
    }