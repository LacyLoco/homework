package com.example.homework2.ui.features.base.holders

import android.view.View
import com.example.homework2.R
import com.example.homework2.model.Likes
import com.example.homework2.model.Post
import com.example.homework2.ui.view.BasePostView

abstract class BasePostViewHolder(itemView: View) : BaseHolder<Post>(itemView) {

    val postView = (itemView as BasePostView)

    init {
        postView.setBackgroundResource(R.color.colorCardBlack)
    }

    open fun bind(post: Post) {
        postView.bind(post)
    }

    open fun bind(likes: Likes) {
        _item = item.copy(likes = likes)
        postView.bindLikes(likes)
    }
}