package com.example.homework2.ui.features.profile

import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ProgressBar
import androidx.recyclerview.widget.ListAdapter
import com.example.homework2.extension.dp
import com.example.homework2.model.Post
import com.example.homework2.model.User
import com.example.homework2.model.ui.Error
import com.example.homework2.model.ui.FullScreenLoader
import com.example.homework2.model.ui.Loader
import com.example.homework2.model.ui.Retry
import com.example.homework2.ui.TransitionParams
import com.example.homework2.ui.features.base.holders.*
import com.example.homework2.ui.screen.RecyclerItem
import com.example.homework2.ui.view.LoadingProfileShimmerView
import com.example.homework2.ui.view.PostView
import com.example.homework2.ui.view.PostWithImagesView
import com.example.homework2.ui.view.UserProfileView

class UserProfileAdapter(
    private val itemClick: (Post, View) -> Unit,
    private val commentClick: (Post, View) -> Unit,
    private val repostClick: (Post) -> Unit,
    private val saveImageClick: (Post) -> Unit,
    private val retryClick: () -> Unit
) : ListAdapter<RecyclerItem, BaseHolder<*>>(ItemDiffer) {

    override fun getItemViewType(position: Int): Int =
        when (val item = currentList[position]) {
            is Post -> getPostType(item)
            is User -> PROFILE_VIEW
            is Loader -> LOADER_VIEW
            is FullScreenLoader -> LOADER_SHIMMER_VIEW
            is Error -> ERROR_VIEW
            is Retry -> RETRY_VIEW
            else -> throw IllegalArgumentException("undefined view type")
        }

    private fun getPostType(post: Post) =
        when (post.image) {
            null -> POST_VIEW
            else -> POST_IMAGE_VIEW
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseHolder<*> =
        when (viewType) {
            POST_VIEW -> initTextViewHolder(parent)
            POST_IMAGE_VIEW -> initTextWithImageViewHolder(parent)
            PROFILE_VIEW -> initProfileViewHolder(parent)
            LOADER_VIEW -> initLoaderViewHolder(parent)
            LOADER_SHIMMER_VIEW -> initLoaderShimmerViewHolder(parent)
            RETRY_VIEW -> RetryViewHolder.create(parent.context, retryClick)
            ERROR_VIEW -> ErrorViewHolder.create(parent.context)
            else -> throw IllegalArgumentException("unresolved viewType: $viewType")
        }

    override fun onBindViewHolder(holder: BaseHolder<*>, position: Int) {
        holder._item = currentList[position]
        holder.itemView.transitionName = TransitionParams.postTransitionName(position)
        when (holder) {
            is TextViewHolder -> holder.bind(currentList[position] as Post)
            is TextWithImageViewHolder -> holder.bind(currentList[position] as Post)
            is ProfileViewHolder -> holder.bind(currentList[position] as User)
        }
    }

    private fun initTextViewHolder(parent: ViewGroup): BaseHolder<*> =
        TextViewHolder(PostView(parent.context), commentClick, itemClick)

    private fun initTextWithImageViewHolder(parent: ViewGroup) =
        TextWithImageViewHolder(
            PostWithImagesView(parent.context),
            itemClick,
            commentClick,
            repostClick,
            saveImageClick
        )

    private fun initProfileViewHolder(parent: ViewGroup) =
        ProfileViewHolder(UserProfileView(parent.context))

    private fun initLoaderViewHolder(parent: ViewGroup) =
        LoaderViewHolder(
            ProgressBar(parent.context).apply {
                layoutParams = FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.MATCH_PARENT,
                    32.dp()
                )
            }
        )

    private fun initLoaderShimmerViewHolder(parent: ViewGroup) =
        LoaderShimmerViewHolder(
            LoadingProfileShimmerView(parent.context).apply {
                layoutParams = FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.MATCH_PARENT,
                    FrameLayout.LayoutParams.MATCH_PARENT
                )
            }
        )

    companion object {
        const val POST_VIEW = 0
        const val POST_IMAGE_VIEW = 1
        const val PROFILE_VIEW = 2
        const val LOADER_VIEW = 3
        const val LOADER_SHIMMER_VIEW = 4
        const val RETRY_VIEW = 5
        const val ERROR_VIEW = 6
    }
}