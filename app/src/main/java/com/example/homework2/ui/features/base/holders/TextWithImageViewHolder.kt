package com.example.homework2.ui.features.base.holders

import android.view.View
import com.example.homework2.model.Post
import com.example.homework2.ui.view.PostWithImagesView

class TextWithImageViewHolder(
    itemView: View,
    itemClick: (Post, View) -> Unit,
    commentClick: (Post, View) -> Unit,
    repostClick: (Post) -> Unit,
    saveImageClick: (Post) -> Unit
) : BasePostViewHolder(itemView) {

    init {
        itemView.setOnClickListener { itemClick.invoke(item, itemView) }
        postView.shareBtn.setOnClickListener { repostClick.invoke(item) }
        postView.commentBtn.setOnClickListener { commentClick.invoke(item, itemView) }
        (postView as PostWithImagesView).apply {
            image.setOnLongClickListener {
                saveImageClick.invoke(item)
                true
            }
            image.setOnClickListener { itemClick.invoke(item, itemView) }
        }
    }
}