package com.example.homework2.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import com.example.homework2.R
import com.example.homework2.extension.calculateViewLeft
import com.example.homework2.extension.calculateViewTop
import com.example.homework2.extension.getHeightWithMargins
import com.example.homework2.extension.getWidthWithMargins
import kotlinx.android.synthetic.main.post_layout.view.*
import kotlin.math.max

class PostView @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyleAttr: Int = 0
) : BasePostView(context, attributeSet, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.post_layout, this, true)
    }

    override val avatarImage: ImageView = postAvatarImage
    override val title: TextView = postTitle
    override val subTitle: TextView = postSubTitle
    override val text: TextView = postText
    override val likeBtn: TextView = postLike
    override val commentBtn: TextView = postComment
    override val shareBtn: TextView = postShare

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val desiredWidth = MeasureSpec.getSize(widthMeasureSpec)
        var height = 0

        //Header
        measureChildWithMargins(avatarImage, widthMeasureSpec, 0, heightMeasureSpec, height)
        measureChildWithMargins(
            title,
            widthMeasureSpec - avatarImage.getWidthWithMargins(),
            0,
            heightMeasureSpec,
            height
        )
        measureChildWithMargins(
            subTitle,
            widthMeasureSpec - avatarImage.getWidthWithMargins(),
            0,
            heightMeasureSpec,
            height
        )

        height += max(
            avatarImage.getHeightWithMargins(),
            title.getHeightWithMargins() + subTitle.getHeightWithMargins()
        )

        //Body
        if (text.isVisible) {
            measureChildWithMargins(text, widthMeasureSpec, 0, heightMeasureSpec, height)
            height += text.getHeightWithMargins()
        }

        //Footer
        measureChildWithMargins(likeBtn, widthMeasureSpec, 0, heightMeasureSpec, height)

        if (commentBtn.isVisible)
            measureChildWithMargins(commentBtn, widthMeasureSpec, 0, heightMeasureSpec, height)

        measureChildWithMargins(shareBtn, widthMeasureSpec, 0, heightMeasureSpec, height)

        height += maxOf(
            likeBtn.getHeightWithMargins(),
            commentBtn.getHeightWithMargins(),
            shareBtn.getHeightWithMargins()
        )

        setMeasuredDimension(desiredWidth, View.resolveSize(height, heightMeasureSpec))
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        var currentLeft = l + paddingLeft
        var currentTop = paddingTop

        //Avatar
        val avatarLeft = avatarImage.calculateViewLeft(currentLeft)
        val avatarTop = avatarImage.calculateViewTop(currentTop)
        val avatarRight = avatarLeft + avatarImage.measuredWidth
        val avatarBottom = avatarTop + avatarImage.measuredHeight

        avatarImage.layout(avatarLeft, avatarTop, avatarRight, avatarBottom)

        //Title
        val titleTextLeft = title.calculateViewLeft(avatarRight)
        val titleTextTop = title.calculateViewTop(currentTop)
        val titleTextBottom = titleTextTop + title.measuredHeight

        title.layout(
            titleTextLeft,
            titleTextTop,
            titleTextLeft + title.measuredWidth,
            titleTextBottom
        )
        currentTop = titleTextBottom

        //Subtitle
        val subtitleTextLeft = subTitle.calculateViewLeft(avatarRight)
        val subtitleTextBottom = currentTop + subTitle.measuredHeight

        subTitle.layout(
            subtitleTextLeft,
            currentTop,
            subtitleTextLeft + subTitle.measuredWidth,
            subtitleTextBottom
        )

        currentTop = max(avatarBottom, subtitleTextBottom)

        //Body text
        if (text.isVisible) {
            val bodyTextLeft = text.calculateViewLeft(currentLeft)
            val bodyTextTop = text.calculateViewTop(currentTop)
            val bodyTextRight = bodyTextLeft + text.measuredWidth
            val bodyTextBottom = bodyTextTop + text.measuredHeight

            text.layout(bodyTextLeft, bodyTextTop, bodyTextRight, bodyTextBottom)
            currentTop = bodyTextBottom
        }

        //Like button
        val likeButtonLeft = likeBtn.calculateViewLeft(currentLeft)
        val likeButtonTop = likeBtn.calculateViewTop(currentTop)
        val likeButtonRight = likeButtonLeft + likeBtn.measuredWidth
        val likeButtonBottom = likeButtonTop + likeBtn.measuredHeight

        likeBtn.layout(likeButtonLeft, likeButtonTop, likeButtonRight, likeButtonBottom)

        currentLeft = likeButtonRight

        //Comment button
        if (commentBtn.isVisible) {
            val commentBtnLeft = commentBtn.calculateViewLeft(currentLeft)
            val commentBtnTop = commentBtn.calculateViewTop(currentTop)
            val commentBtnRight = commentBtnLeft + commentBtn.measuredWidth
            val commentBtnBottom = commentBtnTop + commentBtn.measuredHeight

            commentBtn.layout(commentBtnLeft, commentBtnTop, commentBtnRight, commentBtnBottom)

            currentLeft = commentBtnRight
        }

        //Share button
        val shareBtnLeft = shareBtn.calculateViewLeft(currentLeft)
        val shareBtnTop = shareBtn.calculateViewTop(currentTop)
        val shareBtnRight = shareBtnLeft + shareBtn.measuredWidth
        val shareBtnBottom = shareBtnTop + shareBtn.measuredHeight

        shareBtn.layout(shareBtnLeft, shareBtnTop, shareBtnRight, shareBtnBottom)
    }
}