package com.example.homework2.ui.view

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.example.homework2.R
import com.example.homework2.extension.loadCircle
import com.example.homework2.model.Likes
import com.example.homework2.model.Post

abstract class BasePostView @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ViewGroup(context, attributeSet, defStyleAttr) {

    private val likeIcon = ContextCompat.getDrawable(context, R.drawable.ic_like)
    private val emptyLikeIcon = ContextCompat.getDrawable(context, R.drawable.ic_empty_like)

    abstract val avatarImage: ImageView
    abstract val title: TextView
    abstract val subTitle: TextView
    abstract val text: TextView
    abstract val likeBtn: TextView
    abstract val shareBtn: TextView
    abstract val commentBtn: TextView

    open fun bind(post: Post) {
        avatarImage.loadCircle(post.author.avatar)
        title.text = post.author.name
        subTitle.text = post.textDate
        text.text = post.text
        text.isVisible = post.text.isNullOrEmpty().not()
        shareBtn.text = post.shareBtn.toString()
        commentBtn.isVisible = post.comments.can_post
        commentBtn.text = post.comments.count.toString().takeIf { post.comments.can_post }.orEmpty()
        bindLikes(post.likes)
    }

    fun bindLikes(likes: Likes) {
        if (likes.userLikes)
            setLikeIcon(likeIcon)
        else
            setLikeIcon(emptyLikeIcon)

        likeBtn.text = likes.count.toString()
    }

    private fun setLikeIcon(icon: Drawable?) {
        likeBtn.setCompoundDrawablesWithIntrinsicBounds(icon, null, null, null)
    }

    override fun generateDefaultLayoutParams(): LayoutParams =
        MarginLayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)

    override fun generateLayoutParams(attrs: AttributeSet?): LayoutParams =
        MarginLayoutParams(context, attrs)
}