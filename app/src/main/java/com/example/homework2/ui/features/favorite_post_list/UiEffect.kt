package com.example.homework2.ui.features.favorite_post_list

sealed class UiEffect {

    object PostsIsEmpty : UiEffect()
}