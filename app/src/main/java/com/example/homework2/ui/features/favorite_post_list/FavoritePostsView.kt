package com.example.homework2.ui.features.favorite_post_list

interface FavoritePostsView {
    fun render(state: State)

    fun handleUiEffect(uiEffect: UiEffect)
}
