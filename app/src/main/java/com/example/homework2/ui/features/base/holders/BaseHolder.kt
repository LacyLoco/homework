package com.example.homework2.ui.features.base.holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseHolder<T>(itemView: View) : RecyclerView.ViewHolder(itemView) {
    lateinit var _item: Any
    val item: T
        @Suppress("UNCHECKED_CAST")
        get() = _item as T
}