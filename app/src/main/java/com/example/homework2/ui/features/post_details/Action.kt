package com.example.homework2.ui.features.post_details

import com.example.homework2.model.Comment

sealed class Action {
    //UI actions
    class LoadFirstComments(val showType: PostFragment.ShowType) : Action()
    object LoadNextComments : Action()
    object UpdateComments : Action()
    object RetryLoadComments : Action()
    class AddComment(val message: String) : Action()

    //Presenter actions
    data class CommentsLoaded(val comments: List<Comment>) : Action()
    object CommentAddedComplete : Action()
    object StartLoadingNextPage : Action()
    object EndLoadingNextPage : Action()
    object ErrorCommentLoading : Action()
}