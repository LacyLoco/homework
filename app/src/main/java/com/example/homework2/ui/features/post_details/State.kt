package com.example.homework2.ui.features.post_details

import com.example.homework2.model.COMMENT_NO_ID
import com.example.homework2.model.Post
import com.example.homework2.model.ui.Loader
import com.example.homework2.model.ui.Retry
import com.example.homework2.ui.screen.RecyclerItem

data class State(
    val post: Post,
    val lastCommentId: Int = COMMENT_NO_ID,
    val items: List<RecyclerItem> = emptyList(),
    val sendingMessageEnabled: Boolean = false
)

internal fun State.reduce(action: Action): State =
    when (action) {
        is Action.LoadFirstComments -> copy(
            lastCommentId = COMMENT_NO_ID,
            items = listOf(post, Loader)
        )
        is Action.UpdateComments -> copy(
            lastCommentId = COMMENT_NO_ID,
            items = listOf(post, Loader)
        )
        is Action.ErrorCommentLoading -> copy(
            items = items.filter { it !is Loader && it !is Retry }
                .toMutableList()
                .apply {
                    add(Retry)
                }
        )
        is Action.StartLoadingNextPage ->
            copy(
                items = items.filter { it !is Loader && it !is Retry }
                    .toMutableList()
                    .apply {
                        add(Loader)
                    }
            )
        is Action.EndLoadingNextPage ->
            copy(
                items = items.filter { it !is Loader && it !is Retry }
            )
        is Action.CommentsLoaded -> copy(
            lastCommentId = action.comments.lastOrNull()?.id ?: COMMENT_NO_ID,
            items = mutableListOf<RecyclerItem>(
                post
            ).apply {
                addAll(action.comments)
            }
        )
        is Action.CommentAddedComplete -> copy(
            sendingMessageEnabled = true
        )
        is Action.AddComment -> copy(
            sendingMessageEnabled = false
        )
        else -> this
    }