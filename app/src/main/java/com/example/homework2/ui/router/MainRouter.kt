package com.example.homework2.ui.router

import android.view.View
import com.example.homework2.model.Post

interface MainRouter {
    fun showPostDetailsScreen(post: Post, sharedView: View)
    fun showPostCommentsScreen(post: Post, sharedView: View)
    fun showCreatePostScreen()
    fun showMainScreen()
    fun showAuthScreen()
}