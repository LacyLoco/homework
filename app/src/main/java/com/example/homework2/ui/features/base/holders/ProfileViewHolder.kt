package com.example.homework2.ui.features.base.holders

import android.view.View
import com.example.homework2.R
import com.example.homework2.model.User
import com.example.homework2.ui.view.UserProfileView

class ProfileViewHolder(itemView: View) : BaseHolder<User>(itemView) {

    private val userProfileView = (itemView as UserProfileView)

    init {
        userProfileView.setBackgroundResource(R.color.colorCardBlack)
    }

    fun bind(user: User) {
        userProfileView.bind(user)
    }
}