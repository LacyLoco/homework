package com.example.homework2.ui.features.news

sealed class UiEffect {

    class Error(val error: Throwable) : UiEffect()

    object UnAuth : UiEffect()
}