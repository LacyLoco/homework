package com.example.homework2.ui.features.post_details

import com.example.homework2.data.repository.comments.CommentRepository
import com.example.homework2.di.post.PostScope
import com.example.homework2.extension.fromIoThread
import com.example.homework2.extension.toObservable
import com.example.homework2.model.COMMENT_NO_ID
import com.example.homework2.model.Post
import com.example.homework2.ui.base.RxPresenter
import com.freeletics.rxredux.SideEffect
import com.freeletics.rxredux.reduxStore
import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import javax.inject.Inject

private typealias CommentsSideEffect = SideEffect<State, out Action>

@PostScope
class PostPresenter @Inject constructor(
    private val commentRepository: CommentRepository
) : RxPresenter<PostDetailView>() {

    lateinit var post: Post

    private val inputRelay: Relay<Action> = PublishRelay.create()
    private val uiEffectsRelay = PublishRelay.create<UiEffect>()

    val input: Consumer<Action> get() = inputRelay
    private val uiEffectsInput: Observable<UiEffect> get() = uiEffectsRelay

    private val sideEffects = listOf(
        loadFirstComments(),
        loadNextComments(),
        updateComments(),
        addComment(),
        retryLoadComments()
    )

    override fun attachView(view: PostDetailView) {
        super.attachView(view)
        inputRelay.reduxStore(
            initialState = State(post = post, items = listOf(post)),
            sideEffects = sideEffects,
            reducer = State::reduce
        ).observeOn(AndroidSchedulers.mainThread())
            .distinctUntilChanged()
            .subscribe(view::render)
            .disposeOnFinish()
        uiEffectsInput.observeOn(AndroidSchedulers.mainThread())
            .subscribe(view::handleUiEffect)
            .disposeOnFinish()
    }

    private fun loadFirstComments(): CommentsSideEffect = { actions, state ->
        actions.ofType(Action.LoadFirstComments::class.java)
            .doOnNext { action ->
                if (action.showType == PostFragment.ShowType.COMMENT)
                    uiEffectsRelay.accept(UiEffect.ShowComments)
            }
            .switchMap {
                loadComments(post)
            }
    }

    private fun loadNextComments(): CommentsSideEffect = { actions, state ->
        actions.ofType(Action.LoadNextComments::class.java)
            .filter { state().lastCommentId != COMMENT_NO_ID }
            .map { state().lastCommentId }
            .distinctUntilChanged()
            .switchMap { lastCommentId ->
                loadNextComments(post, lastCommentId)
            }
    }

    private fun retryLoadComments(): CommentsSideEffect = { actions, state ->
        actions.ofType(Action.RetryLoadComments::class.java)
            .switchMap {
                if (state().lastCommentId != COMMENT_NO_ID)
                    loadNextComments(post, state().lastCommentId)
                else
                    loadComments(post)
            }
    }

    private fun updateComments(): CommentsSideEffect = { actions, state ->
        actions.ofType(Action.UpdateComments::class.java)
            .switchMap {
                loadAndUpdateComments(state().post)
            }
    }

    private fun loadAndUpdateComments(post: Post): Observable<Action> =
        commentRepository.updateComments(post)
            .toObservable()
            .fromIoThread()
            .map { Action.CommentsLoaded(comments = it) as Action }
            .startWith(Action.StartLoadingNextPage)
            .onErrorReturn { error ->
                uiEffectsRelay.accept(UiEffect.Error(error))
                Action.ErrorCommentLoading
            }

    private fun addComment(): CommentsSideEffect = { actions, state ->
        actions.ofType(Action.AddComment::class.java)
            .switchMap {
                addComment(state().post, it.message)
            }
    }

    private fun loadComments(post: Post): Observable<out Action> =
        commentRepository.getComments(post)
            .fromIoThread()
            .flatMap { comments ->
                if (comments.isEmpty())
                    loadAndUpdateComments(post)
                else Action.CommentsLoaded(comments = comments).toObservable()
            }

    private fun loadNextComments(
        post: Post,
        startFrom: Int
    ): Observable<out Action> =
        commentRepository.getNextComments(post, startFrom)
            .toObservable()
            .fromIoThread()
            .map { Action.EndLoadingNextPage as Action }
            .startWith(Action.StartLoadingNextPage)
            .onErrorReturn { error ->
                uiEffectsRelay.accept(UiEffect.Error(error))
                Action.ErrorCommentLoading
            }

    private fun addComment(post: Post, message: String): Observable<out Action> =
        commentRepository.addComment(post, message)
            .toObservable()
            .fromIoThread()
            .map { Action.CommentAddedComplete }
            .onErrorReturn { error ->
                uiEffectsRelay.accept(UiEffect.Error(error))
                Action.CommentAddedComplete
            }
}