package com.example.homework2.ui.features.base.holders

import android.content.Context
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import com.example.homework2.model.ui.Retry
import com.example.homework2.ui.view.RetryView

class RetryViewHolder(
    itemView: View,
    itemClick: () -> Unit
) : BaseHolder<Retry>(itemView) {

    init {
        itemView.setOnClickListener { itemClick.invoke() }
    }

    companion object {
        fun create(context: Context, itemClick: () -> Unit) =
            RetryViewHolder(RetryView(context).apply {
                layoutParams = FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.MATCH_PARENT,
                    FrameLayout.LayoutParams.WRAP_CONTENT,
                ).apply {
                    gravity = Gravity.CENTER_HORIZONTAL
                }
            }, itemClick)
    }
}