package com.example.homework2.ui.features.post_details

sealed class UiEffect {

    class Error(val error: Throwable) : UiEffect()
    object ShowComments : UiEffect()
}