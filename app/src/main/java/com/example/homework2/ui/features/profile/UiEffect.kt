package com.example.homework2.ui.features.profile

sealed class UiEffect {
    class Error(val error: Throwable) : UiEffect()
}