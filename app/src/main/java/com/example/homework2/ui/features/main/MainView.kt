package com.example.homework2.ui.features.main

interface MainView {
    fun render(state: State)
}