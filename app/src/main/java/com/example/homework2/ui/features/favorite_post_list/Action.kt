package com.example.homework2.ui.features.favorite_post_list

import com.example.homework2.model.Post

sealed class Action {
    //UI actions
    object LoadFavoritePosts : Action()
    object RetryLoadFavoritePosts : Action()
    class DislikePost(val post: Post) : Action()

    //Presenter actions
    data class FavoritePostsLoaded(val posts: List<Post>) : Action()
    object Empty : Action()
}
