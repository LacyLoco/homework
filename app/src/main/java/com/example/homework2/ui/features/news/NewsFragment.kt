package com.example.homework2.ui.features.news

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ItemTouchHelper
import com.example.homework2.R
import com.example.homework2.extension.errorDialog
import com.example.homework2.extension.injector
import com.example.homework2.extension.toggleLike
import com.example.homework2.model.Post
import com.example.homework2.ui.base.BaseFragment
import com.example.homework2.ui.helpers.DividerTextItemDecoration
import com.example.homework2.ui.helpers.SwipeHelper
import com.example.homework2.ui.helpers.addPaginator
import com.example.homework2.ui.helpers.convertToString
import com.example.homework2.ui.router.MainRouter
import com.example.homework2.ui.screen.HomeScreen
import kotlinx.android.synthetic.main.fragment_post_list.*
import javax.inject.Inject

class NewsFragment : BaseFragment<NewsView, NewsPresenter>(), NewsView, HomeScreen {

    @Inject
    override lateinit var presenter: NewsPresenter

    private val postAdapter =
        NewsAdapter(
            ::showPostDetailScreen,
            ::commentPost,
            ::shareImage,
            ::showAddToGalleryDialog,
            ::retryClick
        )

    override fun injectComponent() =
        injector.newsComponent().inject(this)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_post_list, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        newsList.apply {
            adapter = postAdapter
            addItemDecoration(
                DividerTextItemDecoration(
                    this@NewsFragment.requireContext(),
                    postAdapter::getDecoratorTextForPosition
                )
            )
            setHasFixedSize(true)
            addPaginator { presenter.input.accept(Action.LoadNextPosts) }
        }

        val callback = SwipeHelper(::removeItem, ::likeItem)
        val touchHelper = ItemTouchHelper(callback)
        touchHelper.attachToRecyclerView(newsList)

        swipeRefreshLayout.setOnRefreshListener {
            presenter.input.accept(Action.UpdatePosts)
            swipeRefreshLayout.isRefreshing = false
        }

        postAdapter.submitList(presenter.posts)
        presenter.input.accept(Action.LoadFirstPosts)
    }

    override fun authSuccess() {
        presenter.input.accept(Action.LoadFirstPosts)
    }

    override fun render(state: State) {
        postAdapter.submitList(state.items)
    }

    override fun handleUiEffect(uiEffect: UiEffect) {
        when (uiEffect) {
            is UiEffect.Error -> requireContext().errorDialog(
                uiEffect.error.convertToString(
                    requireContext()
                )
            )
            is UiEffect.UnAuth -> showAuthScreen()
        }
    }

    private fun removeItem(position: Int) {
        presenter.input.accept(Action.RemovePost(position))
    }

    private fun likeItem(position: Int) {
        val post = postAdapter.currentList[position] as Post

        if (post.toggleLike())
            presenter.input.accept(Action.LikePost(post))
        else
            presenter.input.accept(Action.DislikePost(post))
        postAdapter.notifyItemChanged(position, post.likes)
    }

    private fun showPostDetailScreen(post: Post, view: View) {
        (requireContext() as? MainRouter)?.showPostDetailsScreen(post, view)
    }

    private fun commentPost(post: Post, view: View) {
        (requireContext() as? MainRouter)?.showPostCommentsScreen(post, view)
    }

    private fun showAuthScreen() {
        (requireContext() as? MainRouter)?.showAuthScreen()
    }

    private fun retryClick() {
        presenter.input.accept(Action.RetryLoadPosts)
    }

    companion object {
        fun newInstance(): NewsFragment =
            NewsFragment()

    }
}