package com.example.homework2.ui.features.base.holders

import android.view.View
import com.example.homework2.model.Post

class TextViewHolder(
    itemView: View,
    commentClick: (Post, View) -> Unit,
    itemClick: (Post, View) -> Unit
) : BasePostViewHolder(itemView) {

    init {
        itemView.setOnClickListener { itemClick.invoke(item, itemView) }
        postView.commentBtn.setOnClickListener { commentClick.invoke(item, itemView) }
    }
}