package com.example.homework2.ui.features.profile

import com.example.homework2.ui.screen.RecyclerItem

sealed class Action {
    //UI actions
    object LoadProfileAndWall : Action()
    object LoadNextWallPosts : Action()
    object UpdateProfileAndWallPosts : Action()
    object RetryLoadProfileAndWall : Action()

    //Presenter actions
    class ProfileAndWallLoaded(val items: List<RecyclerItem>) : Action()
    object StartLoadingNextPage : Action()
    object EndLoadingNextPage : Action()
    object ErrorPostLoading : Action()
    class Error(val error: Throwable) : Action()
}