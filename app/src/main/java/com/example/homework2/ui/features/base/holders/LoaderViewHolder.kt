package com.example.homework2.ui.features.base.holders

import android.view.View
import com.example.homework2.model.ui.Loader

class LoaderViewHolder(itemView: View) : BaseHolder<Loader>(itemView)