package com.example.homework2.ui.features.create_post

data class State(
    val loading: Boolean = false
)

internal fun State.reduce(action: Action): State =
    when (action) {
        is Action.PostAddedComplete -> copy(
            loading = false
        )
        is Action.AddPost -> copy(
            loading = true
        )
    }