package com.example.homework2.ui.base

interface Presenter<View> {

    fun attachView(view: View)

    fun detachView(isFinishing: Boolean)
}