package com.example.homework2.ui.helpers

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.homework2.extension.orZero

private class Paginator(
    private val itemPoolCount: Int = 5,
    private val readyToNext: () -> Unit
) : RecyclerView.OnScrollListener() {

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        val currentItemCount = recyclerView.adapter?.itemCount.orZero()
        if (currentItemCount < itemPoolCount) return
        if ((recyclerView.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
            > currentItemCount - itemPoolCount
        ) {
            readyToNext.invoke()
        }
    }
}

fun RecyclerView.addPaginator(readyToNext: () -> Unit) {
    addOnScrollListener(Paginator(readyToNext = readyToNext))
}