package com.example.homework2.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.widget.FrameLayout
import androidx.appcompat.widget.AppCompatTextView
import com.example.homework2.R
import com.example.homework2.extension.dp

class RetryView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : FrameLayout(context, attrs) {

    init {
        val text = AppCompatTextView(context).apply {
            setBackgroundResource(R.drawable.bg_rectangle_rounded)
            setPadding(8.dp(), 4.dp(), 8.dp(), 4.dp())
            setTextColor(context.getColor(R.color.colorWhite))
            setText(R.string.retryAgain)
            layoutParams = LayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT,
            ).apply {
                gravity = Gravity.CENTER
            }
        }
        addView(text)
    }
}