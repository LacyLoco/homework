package com.example.homework2.ui.features.create_post

sealed class Action {
    //UI actions
    class AddPost(val message: String) : Action()

    //Presenter actions
    object PostAddedComplete : Action()
}