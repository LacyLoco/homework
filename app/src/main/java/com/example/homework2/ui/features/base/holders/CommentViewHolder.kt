package com.example.homework2.ui.features.base.holders

import android.view.View
import com.example.homework2.model.Comment
import com.example.homework2.ui.view.CommentView

class CommentViewHolder(itemView: View) : BaseHolder<Comment>(itemView) {

    private val commentView = (itemView as CommentView)

    fun bind(comment: Comment) {
        commentView.bind(comment)
    }
}

