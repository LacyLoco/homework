package com.example.homework2.ui.features.main

sealed class Action {
    //UI actions
    object CheckHasFavoritePosts : Action()
    class SaveUserToken(val accessToken: String) : Action()

    //Presenter actions
    data class HasFavoritePosts(val hasPosts: Boolean) : Action()
    object Empty : Action()
}