package com.example.homework2.ui.features.favorite_post_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ItemTouchHelper
import com.example.homework2.R
import com.example.homework2.extension.injector
import com.example.homework2.extension.toggleLike
import com.example.homework2.model.Post
import com.example.homework2.ui.base.BaseFragment
import com.example.homework2.ui.helpers.DividerTextItemDecoration
import com.example.homework2.ui.helpers.SwipeHelper
import com.example.homework2.ui.router.MainRouter
import com.example.homework2.ui.screen.FavoriteScreen
import kotlinx.android.synthetic.main.fragment_favorite_posts.*
import javax.inject.Inject

class FavoritePostsFragment : BaseFragment<FavoritePostsView, FavoritePostsPresenter>(),
    FavoritePostsView,
    FavoriteScreen {

    @Inject
    override lateinit var presenter: FavoritePostsPresenter

    private val postAdapter =
        FavouritePostsAdapter(
            ::showPostDetailScreen,
            ::commentPost,
            ::shareImage,
            ::showAddToGalleryDialog
        )

    override fun injectComponent() =
        injector.favoritesPostsComponent().inject(this)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_favorite_posts, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        favoritePostList.apply {
            adapter = postAdapter
            addItemDecoration(
                DividerTextItemDecoration(
                    this@FavoritePostsFragment.requireContext(),
                    postAdapter::getDecoratorTextForPosition
                )
            )
            setHasFixedSize(true)
        }

        val callback = SwipeHelper(::likeItem, ::likeItem)
        val touchHelper = ItemTouchHelper(callback)
        touchHelper.attachToRecyclerView(favoritePostList)

        presenter.input.accept(Action.LoadFavoritePosts)
    }

    override fun render(state: State) {
        postAdapter.submitList(state.posts)
    }

    override fun handleUiEffect(uiEffect: UiEffect) {
        (requireContext() as? MainRouter?)?.showMainScreen()
    }

    private fun likeItem(position: Int) {
        val post = postAdapter.currentList[position] as Post
        if (post.toggleLike().not())
            presenter.input.accept(Action.DislikePost(post))
    }

    private fun showPostDetailScreen(post: Post, view: View) {
        (requireContext() as? MainRouter)?.showPostDetailsScreen(post, view)
    }

    private fun commentPost(post: Post, view: View) {
        (requireContext() as? MainRouter)?.showPostCommentsScreen(post, view)
    }

    companion object {
        fun newInstance(): FavoritePostsFragment = FavoritePostsFragment()
    }
}