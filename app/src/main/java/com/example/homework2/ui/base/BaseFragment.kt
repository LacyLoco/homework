package com.example.homework2.ui.base

import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.homework2.R
import com.example.homework2.data.repository.gallery.Gallery
import com.example.homework2.extension.addToGalleryDialog
import com.example.homework2.extension.showShareImageScreen
import com.example.homework2.model.Post
import io.reactivex.disposables.CompositeDisposable
import java.io.File
import javax.inject.Inject

abstract class BaseFragment<View, Presenter : BasePresenter<View>> : Fragment() {

    @Inject
    lateinit var gallery: Gallery
    private val safeSubscribe = CompositeDisposable()

    abstract var presenter: Presenter

    fun showAddToGalleryDialog(post: Post) {
        requireContext().addToGalleryDialog {
            addToGallery(post)
        }
    }

    fun shareImage(post: Post) {
        post.image ?: return
        safeSubscribe.add(
            gallery.saveImageFromUrl(post.image)
                .subscribe(::showShareImageScreen, ::showErrorToast)
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        injectComponent()
        super.onCreate(savedInstanceState)
    }

    @Suppress("UNCHECKED_CAST")
    override fun onViewCreated(view: android.view.View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this as View)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        safeSubscribe.clear()
        val isFinishing = isRemoving || requireActivity().isFinishing
        presenter.detachView(isFinishing)
    }

    override fun onDestroy() {
        super.onDestroy()
        releaseComponent()
    }

    open fun injectComponent() = Unit
    open fun releaseComponent() = Unit

    private fun addToGallery(post: Post) {
        post.image ?: return
        safeSubscribe.add(
            gallery.addImageToGallery(post.image)
                .subscribe(::showSuccessSavedToast, ::showErrorToast)
        )
    }

    private fun showShareImageScreen(image: File) {
        requireActivity().showShareImageScreen(image)
    }

    private fun showSuccessSavedToast() {
        Toast.makeText(requireContext(), getString(R.string.successSaved), Toast.LENGTH_SHORT)
            .show()
    }

    private fun showErrorToast(t: Throwable) {
        Toast.makeText(
            requireContext(),
            this.getString(R.string.toastError, t.message), Toast.LENGTH_SHORT
        ).show()
    }
}