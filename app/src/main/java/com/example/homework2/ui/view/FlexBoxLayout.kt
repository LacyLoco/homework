package com.example.homework2.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import androidx.core.view.children
import kotlin.math.max

class FlexBoxLayout @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ViewGroup(context, attributeSet, defStyleAttr) {

    init {
        setWillNotDraw(true)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val desiredWidth = MeasureSpec.getSize(widthMeasureSpec)
        var height = 0
        var currentRowWidth = 0
        var currentRowHeight = 0

        children.forEach { child ->
            measureChildWithMargins(child, widthMeasureSpec, 0, heightMeasureSpec, height)
            currentRowWidth += child.measuredWidth

            if (currentRowWidth > desiredWidth) {
                height += currentRowHeight
                currentRowWidth = child.measuredWidth
                currentRowHeight = child.measuredHeight
            } else {
                currentRowHeight = max(currentRowHeight, child.measuredHeight)
            }
        }
        height += currentRowHeight

        setMeasuredDimension(desiredWidth, resolveSize(height, heightMeasureSpec))
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        var currentLeft = paddingLeft
        var currentTop = t + paddingTop
        var currentRowHeight = 0

        children.forEach { child ->
            var currentRight = currentLeft + child.measuredWidth
            if (currentRight > measuredWidth) {
                currentLeft = paddingLeft
                currentRight = currentLeft + child.measuredWidth
                currentTop += currentRowHeight
                currentRowHeight = child.measuredHeight
            } else
                currentRowHeight = max(currentRowHeight, child.measuredHeight)

            child.layout(currentLeft, currentTop, currentRight, currentTop + child.measuredHeight)
            currentLeft += child.measuredWidth
        }
    }

    override fun generateDefaultLayoutParams(): LayoutParams =
        MarginLayoutParams(WRAP_CONTENT, WRAP_CONTENT)

    override fun generateLayoutParams(attrs: AttributeSet?): LayoutParams =
        MarginLayoutParams(context, attrs)
}