package com.example.homework2.extension

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

fun <T> T.toObservable(): Observable<T> = Observable.just(this)
fun <T> T.toSingle(): Single<T> = Single.just(this)

fun <T> Observable<T>.fromIoThread(): Observable<T> = this.subscribeOn(Schedulers.io())
fun <T> Observable<T>.toMainThread(): Observable<T> = this.observeOn(AndroidSchedulers.mainThread())

fun <T> Single<T>.fromIoThread(): Single<T> = this.subscribeOn(Schedulers.io())
fun <T> Single<T>.toMainThread(): Single<T> = this.observeOn(AndroidSchedulers.mainThread())