package com.example.homework2.extension

fun Int?.orZero(): Int = this ?: 0

fun <T> T?.isNotNull(): Boolean = this != null

fun <T> T?.isNull(): Boolean = this == null