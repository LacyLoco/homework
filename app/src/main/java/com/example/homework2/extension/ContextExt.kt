package com.example.homework2.extension

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.net.Uri
import androidx.appcompat.app.AlertDialog
import androidx.core.content.FileProvider
import com.example.homework2.R
import java.io.File

fun Context.errorDialog(message: String?) {
    val builder: AlertDialog.Builder = AlertDialog.Builder(this)
    builder.setTitle(R.string.errorDialogTitle)
        .setMessage(this.getString(R.string.errorDialogSubTitle, message))
        .setPositiveButton(
            R.string.errorDialogBtn
        ) { dialog, _ ->
            dialog.dismiss()
        }
    builder.create().show()
}

fun Context.addToGalleryDialog(onAccept: () -> Unit) {
    val builder: AlertDialog.Builder = AlertDialog.Builder(this)
    builder.setTitle(R.string.addToGalleryDialogTitle)
        .setMessage(this.getString(R.string.addToGallerySubTitle))
        .setPositiveButton(
            R.string.addToGalleryDialogBtn
        ) { dialog, _ ->
            onAccept.invoke()
            dialog.dismiss()
        }
        .setNegativeButton(R.string.addToGalleryDialogNegativeBtn) { dialog, _ ->
            dialog.dismiss()
        }
    builder.create().show()
}

fun Context.showShareImageScreen(image: File) {
    val imageUri: Uri = FileProvider.getUriForFile(
        this,
        "com.example.homework2.provider",
        image
    )

    val shareIntent: Intent = Intent().apply {
        action = Intent.ACTION_SEND
        putExtra(Intent.EXTRA_STREAM, imageUri)
        type = "image/jpeg"
    }

    val chooser = Intent.createChooser(shareIntent, "")

    val resInfoList: List<ResolveInfo> = packageManager
        .queryIntentActivities(chooser, PackageManager.MATCH_DEFAULT_ONLY)

    for (resolveInfo in resInfoList) {
        val packageName = resolveInfo.activityInfo.packageName
        grantUriPermission(
            packageName,
            imageUri,
            Intent.FLAG_GRANT_READ_URI_PERMISSION
        )
    }
    startActivity(chooser)
}