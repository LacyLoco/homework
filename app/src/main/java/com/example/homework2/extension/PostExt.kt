package com.example.homework2.extension

import com.example.homework2.model.Post

fun Post.toggleLike(): Boolean {
    if (likes.userLikes)
        likes.count--
    else
        likes.count++
    likes.userLikes = likes.userLikes.not()

    return likes.userLikes
}