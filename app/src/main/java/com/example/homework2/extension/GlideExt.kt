package com.example.homework2.extension

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.homework2.R

fun ImageView.load(url: String?) =
    withGlide(url)
        .into(this)

fun ImageView.loadCircle(url: String?) =
    withGlide(url)
        .circleCrop()
        .into(this)

private fun ImageView.withGlide(url: String?) =
    Glide.with(this).load(url).error(R.drawable.ic_student)