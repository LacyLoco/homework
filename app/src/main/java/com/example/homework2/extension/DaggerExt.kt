package com.example.homework2.extension

import android.app.Activity
import androidx.fragment.app.Fragment
import com.example.homework2.di.Injector

val Fragment.injector: Injector
    get() = requireContext().applicationContext as Injector

val Activity.injector: Injector
    get() = applicationContext as Injector