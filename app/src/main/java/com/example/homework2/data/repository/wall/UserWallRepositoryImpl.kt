package com.example.homework2.data.repository.wall

import com.example.homework2.data.api.WallApi
import com.example.homework2.data.converter.convert
import com.example.homework2.data.database.DataBaseController
import com.example.homework2.data.database.entity.PostEntity
import com.example.homework2.extension.toSingle
import com.example.homework2.model.Post
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class UserWallRepositoryImpl @Inject constructor(
    private val wallApi: WallApi,
    dataBaseController: DataBaseController
) : UserWallRepository {

    private val postDao = dataBaseController.postDao

    override fun getUserWallPosts(): Observable<List<Post>> =
        getWallPostsFromDB()
            .flatMapSingle { posts ->
                if (posts.isEmpty())
                    getWallPostsFromNetwork()
                else posts.toSingle()
            }

    override fun getNextUserWallPosts(offset: Int): Single<List<Post>> =
        getWallPostsFromNetwork(offset)


    override fun updatePosts(): Single<List<Post>> =
        getWallPostsFromNetwork(clearOldest = true)

    /**
     * adding a post to the wall,
     * then updating the information by requesting the latest posts on the wall
     */
    override fun addPost(text: String): Single<Boolean> =
        wallApi.addPost(message = text)
            .flatMap { getWallPostsFromNetwork() }
            .map { true }

    private fun getWallPostsFromDB(): Observable<List<Post>> =
        postDao.getWallPosts()
            .distinctUntilChanged()
            .map {
                it.map(PostEntity::convert)
            }

    private fun getWallPostsFromNetwork(
        newOffset: Int = 0,
        clearOldest: Boolean = false
    ): Single<List<Post>> =
        wallApi.getUserWall(offset = newOffset)
            .map { it.response.convert(is_wall = true) }
            .doOnSuccess { posts ->
                if (clearOldest)
                    postDao.clearAndAddPosts(posts.map(Post::convert))
                else
                    postDao.addPosts(posts.map(Post::convert))
            }
}