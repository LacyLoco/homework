package com.example.homework2.data.database.entity

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.homework2.model.Career
import com.example.homework2.model.City
import com.example.homework2.model.Country
import java.util.*

@Entity(tableName = "user_table")
data class UserEntity(
    @PrimaryKey
    val user_id: Int,
    val domain: String?,
    val first_name: String,
    val last_name: String,
    val photo: String?,
    val about: String?,
    val birth_date: String?,
    @Embedded(prefix = "city_")
    val city: City?,
    @Embedded(prefix = "country_")
    val country: Country?,
    @Embedded(prefix = "career_")
    val career: Career?,
    val education: String?,
    val followers_count: Int,
    val last_seen: Calendar,
    val created_date: Long = System.currentTimeMillis()
)
