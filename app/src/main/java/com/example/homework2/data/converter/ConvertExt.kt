package com.example.homework2.data.converter

@Throws(IllegalArgumentException::class)
fun <T> T?.convertNotNull(message: String? = null): T =
    this ?: throw ConvertDataException("undefined property, $message")

class ConvertDataException(message: String) : IllegalArgumentException(message)
