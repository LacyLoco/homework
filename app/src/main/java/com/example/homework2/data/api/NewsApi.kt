package com.example.homework2.data.api

import com.example.homework2.data.api.ApiConst.DEFAULT_ITEM_PER_PAGE
import com.example.homework2.data.model.response.PostResponseNW
import com.example.homework2.data.model.response.ResponseNW
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface NewsApi {

    @GET(ApiUrl.GET_NEWS)
    fun getNews(
        @Query("new_offset") new_offset: Int = 0,
        @Query("start_from") start_from: String? = null,
        @Query("count") count: Int = DEFAULT_ITEM_PER_PAGE,
        @Query("filters") filters: String = "post"
    ): Single<ResponseNW<PostResponseNW>>

    @POST(ApiUrl.REMOVE_POST)
    fun removePost(
        @Query("item_id") item_id: Int,
        @Query("owner_id") owner_id: Int,
        @Query("type") type: String = "wall"
    ): Single<ResponseNW<Boolean>>
}