package com.example.homework2.data.repository.likes

import com.example.homework2.model.Post
import io.reactivex.Single

interface LikesRepository {

    fun addLike(post: Post): Single<Boolean>

    fun deleteLike(post: Post): Single<Boolean>
}