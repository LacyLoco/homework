package com.example.homework2.data.database.dao

import androidx.room.*
import com.example.homework2.data.database.entity.PostEntity
import io.reactivex.Observable

@Dao
interface PostDao {

    companion object {
        const val POST_LIFE_TIME = 1000 * 60 * 5 //5 min
    }

    @Transaction
    fun addPosts(posts: List<PostEntity>) {
        posts.forEach { post ->
            val existPost = getPostById(post.id)
            if (existPost != null)
                addPost(
                    post.copy(
                        is_news = if (existPost.is_news) existPost.is_news else post.is_news,
                        is_wall = if (existPost.is_wall) existPost.is_wall else post.is_wall
                    )
                )
            else addPost(post)
        }
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addPost(post: PostEntity)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updatePost(post: PostEntity)

    @Query("DELETE FROM post_table WHERE id = :postId")
    fun deletePost(postId: Int)

    @Query("DELETE FROM post_table WHERE :currentDate - created_date > $POST_LIFE_TIME")
    fun deleteOldPosts(currentDate: Long = System.currentTimeMillis())

    @Transaction
    fun clearAndAddPosts(posts: List<PostEntity>) {
        deleteAllPosts()
        addPosts(posts)
    }

    @Query("DELETE FROM post_table")
    fun deleteAllPosts()

    @Query("SELECT * FROM post_table WHERE is_news = 1 ORDER BY date DESC")
    fun getNewsPosts(): Observable<List<PostEntity>>

    @Query("SELECT * FROM post_table WHERE is_wall = 1 ORDER BY date DESC")
    fun getWallPosts(): Observable<List<PostEntity>>

    @Query("SELECT * FROM post_table WHERE likes_userLikes = 1 ORDER BY date DESC")
    fun getFavoritePosts(): Observable<List<PostEntity>>

    @Query("SELECT * FROM post_table WHERE id = :id")
    fun getPostById(id: Int): PostEntity?
}