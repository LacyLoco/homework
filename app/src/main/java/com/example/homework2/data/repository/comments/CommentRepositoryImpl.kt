package com.example.homework2.data.repository.comments

import com.example.homework2.data.api.CommentApi
import com.example.homework2.data.converter.convert
import com.example.homework2.data.database.DataBaseController
import com.example.homework2.data.database.entity.CommentEntity
import com.example.homework2.model.Comment
import com.example.homework2.model.Post
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class CommentRepositoryImpl @Inject constructor(
    private val commentApi: CommentApi,
    dataBaseController: DataBaseController
) : CommentRepository {

    private val commentDao = dataBaseController.commentDao
    private val postDao = dataBaseController.postDao

    override fun getComments(post: Post): Observable<List<Comment>> =
        getCommentsFromDB(postId = post.id)

    override fun getNextComments(post: Post, startFromId: Int): Single<List<Comment>> =
        getCommentsFromNetwork(post.source_id, post.id, startFromId)

    override fun updateComments(post: Post): Single<List<Comment>> =
        getCommentsFromNetwork(post.source_id, post.id, clearOldest = true)

    /**
     * adding a comment to the post,
     * then updating the information by requesting the latest post comments
     */
    override fun addComment(post: Post, message: String): Single<Boolean> =
        commentApi.addComment(post.source_id, post.id, message)
            .flatMap {
                postDao.updatePost(
                    post.copy(comments = post.comments.copy(count = ++post.comments.count))
                        .convert()
                )
                getCommentsFromNetwork(post.source_id, post.id)
            }
            .map { true }

    private fun getCommentsFromDB(postId: Int): Observable<List<Comment>> =
        commentDao.getComments(postId)
            .map {
                it.map(CommentEntity::convert)
            }

    private fun getCommentsFromNetwork(
        ownerId: Int,
        postId: Int,
        startFrom: Int? = null,
        clearOldest: Boolean = false
    ): Single<List<Comment>> =
        commentApi.getComments(ownerId, postId, startFrom)
            .map { it.response.convert(postId) }
            .doOnSuccess { comments ->
                if (clearOldest)
                    commentDao.clearAndAddComments(comments.map(Comment::convert), postId)
                else
                    commentDao.addComments(comments.map(Comment::convert))
            }
}