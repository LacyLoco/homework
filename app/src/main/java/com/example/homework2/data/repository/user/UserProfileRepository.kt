package com.example.homework2.data.repository.user

import com.example.homework2.model.User
import io.reactivex.Observable
import io.reactivex.Single

interface UserProfileRepository {

    fun getUserProfile(): Observable<User>

    fun updateUserProfile(): Single<User>
}