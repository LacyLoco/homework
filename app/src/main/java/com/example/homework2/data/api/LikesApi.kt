package com.example.homework2.data.api

import com.example.homework2.data.model.response.LikesResponseNW
import com.example.homework2.data.model.response.ResponseNW
import io.reactivex.Single
import retrofit2.http.POST
import retrofit2.http.Query

interface LikesApi {

    @POST(ApiUrl.ADD_LIKES)
    fun addLikes(
        @Query("item_id") item_id: Int,
        @Query("owner_id") owner_id: Int,
        @Query("type") type: String = "post"
    ): Single<ResponseNW<LikesResponseNW>>

    @POST(ApiUrl.DELETE_LIKES)
    fun deleteLikes(
        @Query("item_id") item_id: Int,
        @Query("owner_id") owner_id: Int,
        @Query("type") type: String = "post"
    ): Single<ResponseNW<LikesResponseNW>>
}