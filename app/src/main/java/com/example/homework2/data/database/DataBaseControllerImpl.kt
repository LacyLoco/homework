package com.example.homework2.data.database

import com.example.homework2.data.database.dao.CommentDao
import com.example.homework2.data.database.dao.PostDao
import com.example.homework2.data.database.dao.UserDao
import com.example.homework2.extension.fromIoThread
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class DataBaseControllerImpl @Inject constructor(
    appDataBase: AppDataBase
) : DataBaseController {

    private val compositeDisposable = CompositeDisposable()

    override val postDao: PostDao = appDataBase.postDao()
    override val commentDao: CommentDao = appDataBase.commentDao()
    override val userDao: UserDao = appDataBase.userDao()

    override fun checkRelevanceOfData() {
        compositeDisposable.addAll(
            removeOldPosts().subscribe(),
            removeOldUser().subscribe()
        )
    }

    private fun removeOldPosts(): Single<Unit> =
        Single.fromCallable { postDao.deleteOldPosts() }
            .fromIoThread()

    private fun removeOldUser(): Single<Unit> =
        Single.fromCallable { userDao.deleteOldUser() }
            .fromIoThread()
}