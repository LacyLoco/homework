package com.example.homework2.data.database.dao

import androidx.room.*
import com.example.homework2.data.database.entity.CommentEntity
import io.reactivex.Observable

@Dao
interface CommentDao {

    @Query("SELECT * FROM comment_table WHERE post_id = :postId ORDER BY date DESC")
    fun getComments(postId: Int): Observable<List<CommentEntity>>

    @Transaction
    fun clearAndAddComments(comments: List<CommentEntity>, postId: Int) {
        deleteAllComments(postId)
        addComments(comments)
    }

    @Query("DELETE FROM comment_table WHERE :postId = post_id")
    fun deleteAllComments(postId: Int)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addComments(comments: List<CommentEntity>)
}