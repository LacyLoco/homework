package com.example.homework2.data.model.response

import com.example.homework2.data.model.CommentNW
import com.example.homework2.data.model.GroupNW
import com.example.homework2.data.model.PostNW
import com.example.homework2.data.model.ProfileNW
import com.google.gson.annotations.SerializedName

class ResponseNW<T>(
    @SerializedName("response") val response: T,
    @SerializedName("error") val error: ErrorNW?
)

class ErrorNW(
    @SerializedName("error_code") val error_code: Int?,
    @SerializedName("error_msg") val error_msg: String?
)

class PostResponseNW(
    @SerializedName("items") val items: List<PostNW?>?,
    @SerializedName("profiles") val profiles: List<ProfileNW>?,
    @SerializedName("groups") val groups: List<GroupNW?>?,
    @SerializedName("next_from") val nextFrom: String?
)

class LikesResponseNW(
    @SerializedName("likes") val likes: Int?
)

class CommentsResponseNW(
    @SerializedName("items") val items: List<CommentNW?>?,
    @SerializedName("profiles") val profiles: List<ProfileNW>?,
    @SerializedName("groups") val groups: List<GroupNW?>?,
)

class AddCommentResponseNW(
    @SerializedName("comment_id") val comment_id: Int?
)

class AddPostResponseNW(
    @SerializedName("post_id") val post_id: Int?
)