package com.example.homework2.data.repository.comments

import com.example.homework2.model.Comment
import com.example.homework2.model.Post
import io.reactivex.Observable
import io.reactivex.Single

interface CommentRepository {

    fun getComments(post: Post): Observable<List<Comment>>

    fun updateComments(post: Post): Single<List<Comment>>

    fun getNextComments(post: Post, startFromId: Int): Single<List<Comment>>

    fun addComment(post: Post, message: String): Single<Boolean>
}