package com.example.homework2.data.repository.gallery

import io.reactivex.Completable
import io.reactivex.Single
import java.io.File

interface Gallery {

    fun addImageToGallery(fileUrl: String): Completable

    fun saveImageFromUrl(fileUrl: String): Single<File>
}