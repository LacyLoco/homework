package com.example.homework2.data.api
import com.example.homework2.data.api.ApiConst.DEFAULT_ITEM_PER_PAGE
import com.example.homework2.data.model.response.AddCommentResponseNW
import com.example.homework2.data.model.response.CommentsResponseNW
import com.example.homework2.data.model.response.ResponseNW
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface CommentApi {

    @GET(ApiUrl.GET_COMMENTS)
    fun getComments(
        @Query("owner_id") owner_id: Int,
        @Query("post_id") post_id: Int,
        @Query("start_comment_id") start_comment_id: Int? = null,
        @Query("count") count: Int = DEFAULT_ITEM_PER_PAGE,
        @Query("extended") extended: Int = 1,
        @Query("sort") sort: String = "desc"
    ): Single<ResponseNW<CommentsResponseNW>>

    @POST(ApiUrl.ADD_COMMENT)
    fun addComment(
        @Query("owner_id") owner_id: Int,
        @Query("post_id") post_id: Int,
        @Query("message") message: String
    ): Single<ResponseNW<AddCommentResponseNW>>
}