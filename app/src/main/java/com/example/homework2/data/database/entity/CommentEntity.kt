package com.example.homework2.data.database.entity

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.example.homework2.model.Author
import java.util.*

@Entity(
    tableName = "comment_table",
    foreignKeys = [
        ForeignKey(
            entity = PostEntity::class,
            parentColumns = ["id"],
            childColumns = ["post_id"],
            onDelete = ForeignKey.CASCADE
        )
    ]
)
data class CommentEntity(
    @PrimaryKey
    val id: Int,
    val post_id: Int,
    @Embedded
    val author: Author,
    val date: Calendar,
    val text_date: String,
    val text: String
)