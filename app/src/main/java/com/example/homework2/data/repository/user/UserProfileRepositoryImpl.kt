package com.example.homework2.data.repository.user

import com.example.homework2.data.api.UserApi
import com.example.homework2.data.converter.convert
import com.example.homework2.data.converter.convertNotNull
import com.example.homework2.data.database.DataBaseController
import com.example.homework2.data.database.entity.UserEntity
import com.example.homework2.extension.fromIoThread
import com.example.homework2.extension.toSingle
import com.example.homework2.model.User
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class UserProfileRepositoryImpl @Inject constructor(
    private val userApi: UserApi,
    dataBaseController: DataBaseController
) : UserProfileRepository {

    private val userDao = dataBaseController.userDao

    override fun getUserProfile(): Observable<User> =
        getUserProfileFromDB()
            .flatMapSingle { users ->
                if (users.isEmpty())
                    getUserFromNetwork()
                else users.first().toSingle()
            }

    override fun updateUserProfile(): Single<User> =
        getUserFromNetwork(clearOldest = true)

    private fun getUserProfileFromDB() =
        userDao.getUserProfile()
            .map {
                it.map(UserEntity::convert)
            }

    private fun getUserFromNetwork(
        clearOldest: Boolean = false
    ): Single<User> =
        userApi.getUserProfile()
            .fromIoThread()
            .map { it.response.first().convertNotNull().convert() }
            .doOnSuccess { user ->
                if (clearOldest)
                    userDao.clearAndAddUser(user.convert())
                else
                    userDao.addUserProfile(user.convert())
            }
}