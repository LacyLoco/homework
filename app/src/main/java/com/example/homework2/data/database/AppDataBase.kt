package com.example.homework2.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.homework2.data.database.converter.CalendarConverter
import com.example.homework2.data.database.dao.CommentDao
import com.example.homework2.data.database.dao.PostDao
import com.example.homework2.data.database.dao.UserDao
import com.example.homework2.data.database.entity.CommentEntity
import com.example.homework2.data.database.entity.PostEntity
import com.example.homework2.data.database.entity.UserEntity

@Database(entities = [PostEntity::class, CommentEntity::class, UserEntity::class], version = 15)
@TypeConverters(CalendarConverter::class)
abstract class AppDataBase : RoomDatabase() {

    abstract fun postDao(): PostDao
    abstract fun commentDao(): CommentDao
    abstract fun userDao(): UserDao
}