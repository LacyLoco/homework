package com.example.homework2.data.api

object ApiUrl {

    const val BASE_URL = "https://api.vk.com/"

    private const val METHOD_PREFIX = "method"

    const val GET_NEWS = "$METHOD_PREFIX/newsfeed.get"
    const val REMOVE_POST = "$METHOD_PREFIX/newsfeed.ignoreItem"

    const val ADD_LIKES = "$METHOD_PREFIX/likes.add"
    const val DELETE_LIKES = "$METHOD_PREFIX/likes.delete"

    const val GET_COMMENTS = "$METHOD_PREFIX/wall.getComments"
    const val ADD_COMMENT = "$METHOD_PREFIX/wall.createComment"

    const val GET_USER_PROFILE = "$METHOD_PREFIX/users.get"

    const val GET_USER_WALL = "$METHOD_PREFIX/wall.get"
    const val ADD_POST = "$METHOD_PREFIX/wall.post"
}