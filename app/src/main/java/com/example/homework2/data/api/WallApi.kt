package com.example.homework2.data.api

import com.example.homework2.data.model.response.AddPostResponseNW
import com.example.homework2.data.model.response.PostResponseNW
import com.example.homework2.data.model.response.ResponseNW
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface WallApi {

    @GET(ApiUrl.GET_USER_WALL)
    fun getUserWall(
        @Query("owner_id") owner_id: Int? = null,
        @Query("count") count: Int = ApiConst.DEFAULT_ITEM_PER_PAGE,
        @Query("filter") filter: String = "owner",
        @Query("offset") offset: Int = 0,
        @Query("extended") extended: Int = 1
    ): Single<ResponseNW<PostResponseNW>>

    @POST(ApiUrl.ADD_POST)
    fun addPost(
        @Query("owner_id") owner_id: Int? = null,
        @Query("message") message: String
    ): Single<ResponseNW<AddPostResponseNW>>
}