package com.example.homework2.data.api

import com.example.homework2.data.model.UserNW
import com.example.homework2.data.model.response.ResponseNW
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface UserApi {

    @GET(ApiUrl.GET_USER_PROFILE)
    fun getUserProfile(
        @Query("fields") fields: String = USER_FIELDS
    ): Single<ResponseNW<List<UserNW>>>

    companion object {
        private val userFields = listOf(
            "id",
            "first_name",
            "photo_200",
            "domain",
            "career",
            "last_name",
            "bdate",
            "city",
            "about",
            "last_seen",
            "followers_count",
            "country",
            "education"
        )
        val USER_FIELDS = userFields.joinToString()
    }
}