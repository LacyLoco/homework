package com.example.homework2.data.type_adapter

import com.example.homework2.data.converter.convertNotNull
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import java.lang.reflect.Type
import java.util.*


object DateTypeAdapter : JsonDeserializer<Calendar> {

    @Throws(JsonParseException::class)
    override fun deserialize(
        json: JsonElement,
        typeOfT: Type,
        context: JsonDeserializationContext
    ): Calendar? =
        tryToConvertJson {
            Calendar.getInstance().apply { timeInMillis = json.asLong.convertNotNull() * 1000 }
        }
}