package com.example.homework2.data.type_adapter

fun <T> tryToConvertJson(converter: () -> T?): T? =
    try {
        converter.invoke()
    } catch (t: Throwable) {
        null
    }