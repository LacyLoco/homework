package com.example.homework2.data.database.dao

import androidx.room.*
import com.example.homework2.data.database.entity.UserEntity
import io.reactivex.Observable

@Dao
interface UserDao {

    companion object {
        const val USER_LIFE_TIME = 1000 * 60 * 5 //5 min
    }

    @Query("SELECT * FROM user_table ")
    fun getUserProfile(): Observable<List<UserEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addUserProfile(user: UserEntity)

    @Transaction
    fun clearAndAddUser(user: UserEntity) {
        deleteAllUser()
        addUserProfile(user)
    }

    @Query("DELETE FROM user_table")
    fun deleteAllUser()

    @Query("DELETE FROM user_table WHERE :currentDate - created_date > $USER_LIFE_TIME")
    fun deleteOldUser(currentDate: Long = System.currentTimeMillis())

}