package com.example.homework2.data.repository.storage

interface UserStorageRepository {
    var accessToken: String
}