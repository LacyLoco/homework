package com.example.homework2.data.repository.storage

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import javax.inject.Inject

class UserStorageRepositoryImpl @Inject constructor(
    context: Context
) : UserStorageRepository {
    private val prefDB: SharedPreferences =
        context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)

    override var accessToken: String
        get() = prefDB.getString(ACCESS_TOKEN, "") ?: ""
        set(value) = prefDB.edit { putString(ACCESS_TOKEN, value) }

    companion object {
        private const val SHARED_PREF_NAME = "app_shared_preferences"
        private const val ACCESS_TOKEN = "access_token"
    }
}