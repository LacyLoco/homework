package com.example.homework2.data.api

import com.example.homework2.data.converter.convert
import com.example.homework2.data.converter.convertNotNull
import com.example.homework2.data.model.response.ResponseNW
import com.example.homework2.data.repository.storage.UserStorageRepository
import com.google.gson.Gson
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody
import okhttp3.internal.Util
import okio.Buffer
import okio.BufferedSource
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets

class ApiInterceptor(
    private val userStorageRepository: UserStorageRepository,
    private val gson: Gson
) : Interceptor {

    companion object {
        private const val QUERY_ACCESS_TOKEN = "access_token"
        private const val QUERY_VERSION = "v"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val httpUrl = chain.request().url().newBuilder()
            .addQueryParameter(QUERY_ACCESS_TOKEN, userStorageRepository.accessToken)
            .addQueryParameter(QUERY_VERSION, "5.124")
            .build()
        val request = chain.request().newBuilder().url(httpUrl).build()

        val response = chain.proceed(request)

        response.body()?.let(::parseResponse)

        return response
    }

    @Throws
    private fun parseResponse(responseBody: ResponseBody) {
        val source: BufferedSource = responseBody.source()

        val buffer: Buffer = source.buffer

        val charset: Charset = Util.bomAwareCharset(source, StandardCharsets.UTF_8) ?: return

        val json: String = buffer.clone().readString(charset)

        val responseNW = gson.fromJson(json, ResponseNW::class.java)

        //check response has error
        if (responseNW.error != null)
            responseNW.error.convert()

        //check response is null
        responseNW.response.convertNotNull("response is null")
    }
}