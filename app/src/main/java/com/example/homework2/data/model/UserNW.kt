package com.example.homework2.data.model

import com.google.gson.annotations.SerializedName
import java.util.*

class UserNW(
    @SerializedName("id") val id: Int?,
    @SerializedName("first_name") val first_name: String?,
    @SerializedName("last_name") val last_name: String?,
    @SerializedName("domain") val domain: String?,
    @SerializedName("bdate") val bdate: String?,
    @SerializedName("career") val career: List<CareerNW?>?,
    @SerializedName("city") val city: CityNW?,
    @SerializedName("country") val country: CountryNW?,
    @SerializedName("photo_200") val photo: String?,
    @SerializedName("about") val about: String?,
    @SerializedName("last_seen") val last_seen: LastSeenNW?,
    @SerializedName("followers_count") val followers_count: Int?,
    @SerializedName("university_name") val university_name: String?,
)

class CareerNW(
    @SerializedName("position") val position: String?
)

class CityNW(
    @SerializedName("title") val title: String?
)

class CountryNW(
    @SerializedName("title") val title: String?
)

class LastSeenNW(
    @SerializedName("time") val time: Calendar?
)