package com.example.homework2.data.repository.news

import com.example.homework2.data.repository.likes.LikesRepository
import com.example.homework2.model.Post
import io.reactivex.Observable
import io.reactivex.Single

interface NewsRepository : LikesRepository {

    fun getFavoritePosts(): Observable<List<Post>>

    fun getPosts(): Observable<List<Post>>

    fun getNextPosts(
        newOffset: Int = 0,
        startFrom: String? = null
    ): Observable<List<Post>>

    fun updatePosts(): Observable<List<Post>>

    fun removePost(post: Post): Single<Boolean>
}