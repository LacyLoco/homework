package com.example.homework2.data.repository.gallery

import android.content.ContentResolver
import android.content.ContentValues
import android.content.Context
import android.os.Build
import android.provider.MediaStore
import com.bumptech.glide.Glide
import com.example.homework2.extension.fromIoThread
import com.example.homework2.extension.toMainThread
import io.reactivex.Completable
import io.reactivex.Single
import java.io.File
import java.io.FileInputStream
import javax.inject.Inject

class GalleryImpl @Inject constructor(
    private val context: Context
) : Gallery {

    private val contentResolver: ContentResolver = context.contentResolver

    override fun addImageToGallery(fileUrl: String): Completable =
        saveImageFromUrl(fileUrl)
            .flatMapCompletable(::addImageToGallery)

    override fun saveImageFromUrl(fileUrl: String): Single<File> =
        Single.fromCallable { saveFile(fileUrl) }
            .fromIoThread()
            .toMainThread()

    private fun saveFile(fileUrl: String): File {
        val glideCachedFile =
            Glide.with(context).downloadOnly().load(fileUrl).submit().get()
        return File(
            "${context.cacheDir}/app_images",
            glideCachedFile.nameWithoutExtension + ".jpg"
        ).apply {
            mkdirs()
            glideCachedFile.copyTo(this, true)
        }
    }

    private fun addImageToGallery(image: File): Completable =
        Completable.fromAction {
            val values = ContentValues().apply {
                put(MediaStore.Images.Media.TITLE, image.name)
                put(MediaStore.Images.Media.DISPLAY_NAME, image.nameWithoutExtension)
                put(MediaStore.Images.Media.DESCRIPTION, "")
                put(MediaStore.Images.Media.MIME_TYPE, "image/jpg")
                put(MediaStore.Images.Media.DATE_ADDED, System.currentTimeMillis())
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
                    put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis())
                put(MediaStore.Images.Media.DATA, image.toString())
            }

            val fileStream = FileInputStream(image)
            val imageOut = contentResolver.openOutputStream(
                contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
                    ?: throw NullPointerException("Gallery uri not found")
            ) ?: throw NullPointerException("Gallery image not found")

            fileStream.use { stream ->
                stream.copyTo(imageOut)
            }
        }
}