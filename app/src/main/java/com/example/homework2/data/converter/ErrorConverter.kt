package com.example.homework2.data.converter

import com.example.homework2.data.model.response.ErrorNW

@Throws(Throwable::class)
fun ErrorNW.convert(): Nothing =
    if (error_code == 5)
        throw AuthException()
    else
        throw Throwable(error_msg ?: ";(")

class AuthException : RuntimeException("Authorization is required")