package com.example.homework2.data.converter

import com.example.homework2.data.database.entity.UserEntity
import com.example.homework2.data.model.CareerNW
import com.example.homework2.data.model.CityNW
import com.example.homework2.data.model.CountryNW
import com.example.homework2.data.model.UserNW
import com.example.homework2.model.Career
import com.example.homework2.model.City
import com.example.homework2.model.Country
import com.example.homework2.model.User

fun UserNW.convert(): User =
    User(
        userId = id.convertNotNull(),
        domain = domain,
        firstName = first_name.convertNotNull(),
        lastName = last_name.convertNotNull(),
        photo = photo,
        about = about,
        bDate = bdate,
        city = city?.convert(),
        country = country?.convert(),
        career = career?.firstOrNull()?.convert(),
        education = university_name,
        followersCount = followers_count ?: 0,
        lastSeen = last_seen?.time.convertNotNull(),
        lastSeenText = last_seen?.time.convertNotNull().convertToString()
    )

fun CityNW.convert() = City(
    title = title.convertNotNull()
)

fun CountryNW.convert() = Country(
    title = title.convertNotNull()
)

fun CareerNW.convert() = Career(
    position = position.convertNotNull()
)

fun User.convert() = UserEntity(
    user_id = userId,
    domain = domain,
    first_name = firstName,
    last_name = lastName,
    photo = photo,
    about = about,
    birth_date = bDate,
    city = city,
    country = country,
    career = career,
    education = education,
    followers_count = followersCount,
    last_seen = lastSeen
)

fun UserEntity.convert() = User(
    userId = user_id,
    domain = domain,
    firstName = first_name,
    lastName = last_name,
    photo = photo,
    about = about,
    bDate = birth_date,
    city = city,
    country = country,
    career = career,
    education = education,
    followersCount = followers_count,
    lastSeen = last_seen,
    lastSeenText = last_seen.convertToString()
)