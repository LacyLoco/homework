package com.example.homework2.data.model

import com.google.gson.annotations.SerializedName
import java.util.*

class CommentNW(
    @SerializedName("id") val id: Int?,
    @SerializedName("from_id") val from_id: Int?,
    @SerializedName("owner_id") val owner_id: Int?,
    @SerializedName("date") val date: Calendar?,
    @SerializedName("text") val text: String?
)