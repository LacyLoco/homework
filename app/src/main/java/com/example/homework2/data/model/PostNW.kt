package com.example.homework2.data.model

import com.google.gson.annotations.SerializedName
import java.util.*

class PostNW(
    @SerializedName("source_id") val source_id: Int?,
    @SerializedName("owner_id") val owner_id: Int?,
    @SerializedName("post_id") val post_id: Int?,
    @SerializedName("id") val id: Int?,
    @SerializedName("date") val date: Calendar?,
    @SerializedName("text") val text: String?,
    @SerializedName("attachments") val attachments: List<Attachment?>?,
    @SerializedName("comments") val comments: CommentsNW?,
    @SerializedName("likes") val likes: LikesNW?,
    @SerializedName("reposts") val reposts: Reposts?
)

class CommentsNW(
    @SerializedName("count") val count: Int?,
    @SerializedName("can_post") val can_post: Boolean?
)

class LikesNW(
    @SerializedName("count") val count: Int?,
    @SerializedName("user_likes") val user_likes: Boolean?
)

class Reposts(
    @SerializedName("count") val count: Int?
)

class Attachment(
    @SerializedName("photo") val photo: Photo?
)

class Photo(
    @SerializedName("sizes") val sizes: List<Size?>?
)

class Size(
    @SerializedName("url") val url: String?
)

class GroupNW(
    @SerializedName("id") val id: Int?,
    @SerializedName("name") val name: String?,
    @SerializedName("photo_50") val photo_50: String?,
    @SerializedName("photo_100") val photo_100: String?,
    @SerializedName("photo_200") val photo_200: String?
)

class ProfileNW(
    @SerializedName("id") val id: Int?,
    @SerializedName("first_name") val first_name: String?,
    @SerializedName("last_name") val last_name: String?,
    @SerializedName("photo_50") val photo_50: String?,
    @SerializedName("photo_100") val photo_100: String?
)

