package com.example.homework2.data.converter

import android.content.res.Resources
import com.example.homework2.data.database.entity.PostEntity
import com.example.homework2.data.model.*
import com.example.homework2.data.model.response.PostResponseNW
import com.example.homework2.model.Author
import com.example.homework2.model.Comments
import com.example.homework2.model.Likes
import com.example.homework2.model.Post
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.abs

fun PostResponseNW.convert(
    is_news: Boolean = false,
    is_wall: Boolean = false,
): List<Post> =
    items.convertNotNull().mapNotNull { postNW ->
        try {
            postNW?.convert(
                is_news,
                is_wall,
                getAuthor((postNW.source_id ?: postNW.owner_id).convertNotNull(), profiles, groups),
                nextFrom
            )
        } catch (e: ConvertDataException) {
            e.printStackTrace()
            null
        }
    }

fun PostNW.convert(
    is_news: Boolean = false,
    is_wall: Boolean = false,
    author: Author, nextFrom: String?
): Post =
    Post(
        is_news = is_news,
        is_wall = is_wall,
        id = (post_id ?: id).convertNotNull(),
        source_id = (source_id ?: owner_id).convertNotNull(),
        author = author,
        date = date.convertNotNull(),
        textDate = date.convertNotNull().convertToString(),
        text = text,
        image = attachments?.firstOrNull()?.photo?.sizes?.lastOrNull()?.url,
        likes = likes.convertNotNull().convert(),
        comments = comments.convertNotNull().convert(),
        shareBtn = reposts?.count.convertNotNull(),
        nextFrom = nextFrom
    )

private val today = Calendar.getInstance()
private val textDateFormatter =
    SimpleDateFormat("dd MMMM YYYY", Resources.getSystem().configuration.locales[0])

fun Calendar.convertToString(): String = when {
    today.get(Calendar.DATE) == this.get(Calendar.DATE) -> "Сегодня"
    today.get(Calendar.DATE) - this.get(Calendar.DATE) == 1 -> "Вчера"
    else -> textDateFormatter.format(time)
}

fun getAuthor(source_id: Int, profiles: List<ProfileNW>?, groups: List<GroupNW?>?): Author =
    if (source_id < 0) groups.findGroup(source_id)
    else profiles.findProfile(source_id)

fun LikesNW.convert() = Likes(
    count = count.convertNotNull(),
    userLikes = user_likes.convertNotNull()
)

fun CommentsNW.convert() = Comments(
    count = count.convertNotNull(),
    can_post = can_post.convertNotNull()
)

fun GroupNW.convert() = Author(
    avatar = photo_200 ?: photo_100 ?: photo_50,
    name = name.convertNotNull()
)

fun ProfileNW.convert() = Author(
    avatar = photo_100 ?: photo_50,
    name = "${first_name.convertNotNull()} ${last_name.convertNotNull()}"
)

fun List<GroupNW?>?.findGroup(source_id: Int): Author =
    this?.binarySearchBy(abs(source_id)) { it?.id }
        ?.let { this.getOrNull(it) }
        .convertNotNull()
        .convert()

fun List<ProfileNW?>?.findProfile(source_id: Int): Author =
    this?.binarySearchBy(source_id) { it?.id }
        ?.let { this.getOrNull(it) }
        .convertNotNull()
        .convert()

fun Post.convert() = PostEntity(
    is_news = is_news,
    is_wall = is_wall,
    id = id,
    source_id = source_id,
    author = author,
    date = date,
    text = text,
    image = image,
    likes = likes,
    comments = comments,
    share_btn = shareBtn,
    next_from = nextFrom
)

fun PostEntity.convert() = Post(
    is_news = is_news,
    is_wall = is_wall,
    id = id,
    source_id = source_id,
    author = author,
    date = date,
    textDate = date.convertToString(),
    text = text,
    image = image,
    likes = likes,
    comments = comments,
    shareBtn = share_btn,
    nextFrom = next_from
)