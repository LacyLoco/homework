package com.example.homework2.data.database

import com.example.homework2.data.database.dao.CommentDao
import com.example.homework2.data.database.dao.PostDao
import com.example.homework2.data.database.dao.UserDao

interface DataBaseController {
    val postDao: PostDao
    val commentDao: CommentDao
    val userDao: UserDao
    fun checkRelevanceOfData()
}