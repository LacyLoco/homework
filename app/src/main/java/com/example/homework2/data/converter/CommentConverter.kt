package com.example.homework2.data.converter

import com.example.homework2.data.database.entity.CommentEntity
import com.example.homework2.data.model.CommentNW
import com.example.homework2.data.model.response.CommentsResponseNW
import com.example.homework2.model.Author
import com.example.homework2.model.Comment

fun CommentsResponseNW.convert(postId: Int): List<Comment> =
    items.convertNotNull().mapNotNull { commentNW ->
        try {
            commentNW?.convert(
                postId,
                getAuthor(commentNW.from_id.convertNotNull(), profiles, groups)
            )
        } catch (e: ConvertDataException) {
            e.printStackTrace()
            null
        }
    }

fun CommentNW.convert(postId: Int, author: Author): Comment =
    Comment(
        id = id.convertNotNull(),
        source_id = postId,
        date = date.convertNotNull(),
        textDate = date.convertNotNull().convertToString(),
        text = text.convertNotNull(),
        author = author
    )

fun Comment.convert() = CommentEntity(
    id = id,
    post_id = source_id,
    author = author,
    date = date,
    text_date = textDate,
    text = text
)

fun CommentEntity.convert() = Comment(
    id = id,
    source_id = post_id,
    author = author,
    date = date,
    textDate = text_date,
    text = text,
)