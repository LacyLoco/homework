package com.example.homework2.data.database.entity

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.homework2.model.Author
import com.example.homework2.model.Comments
import com.example.homework2.model.Likes
import java.util.*

@Entity(tableName = "post_table")
data class PostEntity(
    val is_news: Boolean,
    val is_wall: Boolean,
    @PrimaryKey
    val id: Int,
    val source_id: Int,
    @Embedded
    val author: Author,
    val date: Calendar,
    val text: String?,
    val image: String?,
    @Embedded(prefix = "likes_")
    val likes: Likes,
    @Embedded(prefix = "comments_")
    val comments: Comments,
    val share_btn: Int,
    val next_from: String?,
    val created_date: Long = System.currentTimeMillis()
)