package com.example.homework2.data.database.converter

import androidx.room.TypeConverter
import java.util.*

class CalendarConverter {
    @TypeConverter
    fun fromCalendar(calendar: Calendar): Long =
        calendar.timeInMillis

    @TypeConverter
    fun toCalendar(data: Long): Calendar =
        Calendar.getInstance().apply { timeInMillis = data }
}