package com.example.homework2.data.repository.wall

import com.example.homework2.model.Post
import io.reactivex.Observable
import io.reactivex.Single

interface UserWallRepository {

    fun getUserWallPosts(): Observable<List<Post>>

    fun getNextUserWallPosts(offset: Int): Single<List<Post>>

    fun updatePosts(): Single<List<Post>>

    fun addPost(text: String): Single<Boolean>
}