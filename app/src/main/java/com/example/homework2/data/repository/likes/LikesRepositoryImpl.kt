package com.example.homework2.data.repository.likes

import com.example.homework2.data.api.LikesApi
import com.example.homework2.data.converter.convert
import com.example.homework2.data.database.DataBaseController
import com.example.homework2.extension.isNotNull
import com.example.homework2.model.Post
import io.reactivex.Single
import javax.inject.Inject

class LikesRepositoryImpl @Inject constructor(
    private val likesApi: LikesApi,
    dataBaseController: DataBaseController
) : LikesRepository {

    private val postDao = dataBaseController.postDao

    override fun addLike(post: Post): Single<Boolean> =
        likesApi.addLikes(post.id, post.source_id)
            .doOnSuccess {
                val likes = post.likes
                postDao.updatePost(post.copy(likes = likes.copy(userLikes = true)).convert())
            }
            .map { it.response.likes.isNotNull() }

    override fun deleteLike(post: Post): Single<Boolean> =
        likesApi.deleteLikes(post.id, post.source_id)
            .doOnSuccess {
                val likes = post.likes
                postDao.updatePost(post.copy(likes = likes.copy(userLikes = false)).convert())
            }
            .map { it.response.likes.isNotNull() }
}