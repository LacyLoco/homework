package com.example.homework2.data.repository.news

import com.example.homework2.data.api.NewsApi
import com.example.homework2.data.converter.convert
import com.example.homework2.data.converter.convertNotNull
import com.example.homework2.data.database.DataBaseController
import com.example.homework2.data.database.entity.PostEntity
import com.example.homework2.data.repository.likes.LikesRepository
import com.example.homework2.model.Post
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class NewsRepositoryImpl @Inject constructor(
    private val likesRepository: LikesRepository,
    private val newsApi: NewsApi,
    dataBaseController: DataBaseController
) : NewsRepository, LikesRepository by likesRepository {

    private val postDao = dataBaseController.postDao

    override fun getPosts(): Observable<List<Post>> = loadPosts()

    override fun getNextPosts(
        newOffset: Int,
        startFrom: String?
    ): Observable<List<Post>> = getPostsFromNetwork(newOffset, startFrom).toObservable()

    override fun updatePosts(): Observable<List<Post>> =
        getPostsFromNetwork(clearOldest = true).toObservable()

    override fun removePost(post: Post): Single<Boolean> =
        newsApi.removePost(post.id, post.source_id)
            .map { it.response.convertNotNull() }
            .doOnSuccess { postDao.deletePost(post.id) }

    override fun getFavoritePosts(): Observable<List<Post>> =
        postDao.getFavoritePosts()
            .map { it.map(PostEntity::convert) }

    private fun loadPosts(): Observable<List<Post>> =
        getPostsFromDB()

    private fun getPostsFromDB(): Observable<List<Post>> =
        postDao.getNewsPosts()
            .distinctUntilChanged()
            .map {
                it.map(PostEntity::convert)
            }

    private fun getPostsFromNetwork(
        newOffset: Int = 0,
        startFrom: String? = null,
        clearOldest: Boolean = false
    ): Single<List<Post>> =
        newsApi.getNews(newOffset, startFrom)
            .map { it.response.convert(is_news = true) }
            .doOnSuccess { posts ->
                if (clearOldest)
                    postDao.clearAndAddPosts(posts.map(Post::convert))
                else
                    postDao.addPosts(posts.map(Post::convert))
            }
}