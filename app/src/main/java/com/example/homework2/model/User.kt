package com.example.homework2.model

import android.os.Parcelable
import com.example.homework2.ui.screen.RecyclerItem
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class User(
    val userId: Int,
    val domain: String?,
    val firstName: String,
    val lastName: String,
    val photo: String?,
    val about: String?,
    val bDate: String?,
    val city: City?,
    val country: Country?,
    val career: Career?,
    val education: String?,
    val followersCount: Int,
    val lastSeen: Calendar,
    val lastSeenText: String
) : Parcelable, RecyclerItem {

    val fullName
        get() = "$firstName $lastName"
}

@Parcelize
data class City(
    var title: String
) : Parcelable

@Parcelize
data class Country(
    var title: String
) : Parcelable

@Parcelize
data class Career(
    var position: String
) : Parcelable