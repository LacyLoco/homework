package com.example.homework2.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Author(
    val avatar: String?,
    val name: String
) : Parcelable