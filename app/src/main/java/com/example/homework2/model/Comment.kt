package com.example.homework2.model

import android.os.Parcelable
import com.example.homework2.ui.screen.RecyclerItem
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Comment(
    val id: Int,
    val source_id: Int,
    val author: Author,
    val date: Calendar,
    val textDate: String,
    val text: String
) : Parcelable, RecyclerItem

const val COMMENT_NO_ID = -1