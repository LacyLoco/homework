package com.example.homework2.model

import android.os.Parcelable
import com.example.homework2.ui.screen.RecyclerItem
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Post(
    val is_news: Boolean,
    val is_wall: Boolean,
    val id: Int,
    val source_id: Int,
    val author: Author,
    val date: Calendar,
    val textDate: String,
    val text: String?,
    val image: String?,
    val likes: Likes,
    val comments: Comments,
    val shareBtn: Int,
    val nextFrom: String?
) : Parcelable, RecyclerItem

@Parcelize
data class Likes(
    var count: Int,
    var userLikes: Boolean
) : Parcelable

@Parcelize
data class Comments(
    var count: Int,
    var can_post: Boolean
) : Parcelable