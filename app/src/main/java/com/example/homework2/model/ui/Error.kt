package com.example.homework2.model.ui

import com.example.homework2.ui.screen.RecyclerItem

data class Error(val t: Throwable) : RecyclerItem